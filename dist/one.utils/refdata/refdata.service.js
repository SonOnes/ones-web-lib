"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_request_service_1 = require("../utils/common/http.request.service");
var OneConfig = require("../../config");
var RefdataService = (function () {
    function RefdataService(http) {
        this.http = http;
    }
    RefdataService.prototype.searchCounties = function (filter) {
        var result = this.http.post(OneConfig.COUNTRIES_API.apiEndpoint, JSON.stringify(filter))
            .map(function (res) { return res.json(); });
        return result;
    };
    RefdataService.prototype.searchCities = function (filter) {
        var result = this.http.post(OneConfig.CITIES_API.apiEndpoint, JSON.stringify(filter))
            .map(function (res) { return res.json(); });
        return result;
    };
    RefdataService.prototype.searchDistricts = function (filter) {
        var result = this.http.post(OneConfig.DISTRICT_API.apiEndpoint, JSON.stringify(filter))
            .map(function (res) { return res.json(); });
        return result;
    };
    RefdataService.decorators = [
        { type: core_1.Injectable },
    ];
    /** @nocollapse */
    RefdataService.ctorParameters = function () { return [
        { type: http_request_service_1.HttpRequestService, },
    ]; };
    return RefdataService;
}());
exports.RefdataService = RefdataService;
//# sourceMappingURL=refdata.service.js.map