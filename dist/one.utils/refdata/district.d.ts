import { City } from './city';
export declare class District {
    cityCityId: City;
    cityId: number;
    code: string;
    createdOn: string;
    id: number;
    modifiedOn: string;
    name: string;
    postalCode: string;
    private status;
}
