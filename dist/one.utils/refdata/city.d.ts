import { Country } from './country';
export declare class City {
    code: string;
    countryCountryId: Country;
    countryId: number;
    createdOn: string;
    id: number;
    minimumwage: number;
    modifiedOn: string;
    name: string;
    postcode: string;
    private status;
    timezone: string;
}
