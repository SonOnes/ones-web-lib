import { HttpRequestService } from '../utils/common/http.request.service';
import { Observable } from 'rxjs/Observable';
import { OnesPaging } from '../utils/common/one.obj';
import { Country } from './country';
import { CountryFilter } from './country.filter';
import { City } from './city';
import { CityFilter } from './city.filter';
import { District } from './district';
import { DistrictFilter } from './district.filter';
export declare class RefdataService {
    private http;
    constructor(http: HttpRequestService);
    searchCounties(filter: CountryFilter): Observable<OnesPaging<Country>>;
    searchCities(filter: CityFilter): Observable<OnesPaging<City>>;
    searchDistricts(filter: DistrictFilter): Observable<OnesPaging<District>>;
}
