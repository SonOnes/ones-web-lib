import { OnesSort } from '../utils/common/one.obj';
export declare class CountryFilter {
    code: string;
    createdOn: string;
    id: number;
    languageCode: string;
    modifiedOn: string;
    name: string;
    postcode: string;
    status: StatusEnum;
    timezone: string;
    page: number;
    limit: number;
    sortDTOs: OnesSort[];
}
export declare const enum StatusEnum {
    ACTIVE = 0,
    DEACTIVE = 1,
}
