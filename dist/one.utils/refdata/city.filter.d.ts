import { OnesSort } from '../utils/common/one.obj';
export declare class CityFilter {
    code: string;
    countryId: number;
    createdOn: string;
    id: number;
    minimumwage: number;
    modifiedOn: string;
    name: string;
    postcode: string;
    status: StatusEnum[];
    timezone: string;
    page: number;
    limit: number;
    sortDTOs: OnesSort[];
}
export declare const enum StatusEnum {
    ACTIVE = 0,
    DEACTIVE = 1,
}
