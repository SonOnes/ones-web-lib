import { OnesSort } from '../utils/common/one.obj';
export declare class DistrictFilter {
    cityId: number;
    code: string;
    createdOn: string;
    id: number;
    modifiedOn: string;
    name: string;
    postalCode: string;
    status: StatusEnum[];
    page: number;
    limit: number;
    sortDTOs: OnesSort[];
}
export declare const enum StatusEnum {
    ACTIVE = 0,
    DEACTIVE = 1,
}
