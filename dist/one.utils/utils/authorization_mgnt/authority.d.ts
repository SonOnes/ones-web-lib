export declare class Authority {
    id: number;
    name: string;
    clientId: string;
}
