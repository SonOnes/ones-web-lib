import { HttpRequestService } from '../common/http.request.service';
import { Observable } from 'rxjs/Observable';
import { OnesPaging } from '../common/paging.ones.obj';
import { OnesMessage } from '../common/ones.message';
import { UserInformation } from './user.information';
import { UserInformationFilter } from './user.information.filter';
export declare class UserinformationService {
    private http;
    constructor(http: HttpRequestService);
    getFirst1000(): Observable<OnesPaging<UserInformation>>;
    search(userInformationFilter: UserInformationFilter): Observable<OnesPaging<UserInformation>>;
    deleteObj(id: any): Observable<OnesMessage>;
    deleteObjs(id: any): Observable<OnesMessage>;
    getObj(id: any): Observable<UserInformation>;
    save(userInformationSave: UserInformation): Observable<UserInformation>;
}
