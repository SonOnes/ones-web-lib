import { OnesSort } from '../common/ones.sort.obj';
export declare class UserAuthoritiesFilter {
    id: number;
    userId: number;
    authorityId: number;
    clientId: string;
    updatedOnFrom: string;
    updatedOnTo: string;
    updatedBy: string;
    page: number;
    limit: number;
    sortDTOs: OnesSort[];
}
