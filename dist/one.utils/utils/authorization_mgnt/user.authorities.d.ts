export declare class UserAuthorities {
    id: number;
    userId: number;
    authorityId: number;
    clientId: string;
    updatedOn: string;
    updatedBy: string;
}
