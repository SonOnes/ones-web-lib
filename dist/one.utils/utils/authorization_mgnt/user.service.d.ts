import { HttpRequestService } from '../common/http.request.service';
import { Observable } from 'rxjs/Observable';
import { OnesPaging } from '../common/paging.ones.obj';
import { OnesMessage } from '../common/ones.message';
import { User } from './user';
import { UserFilter } from './user.filter';
export declare class UserService {
    private http;
    constructor(http: HttpRequestService);
    getFirst1000(): Observable<OnesPaging<User>>;
    search(userFilter: UserFilter): Observable<OnesPaging<User>>;
    searchForTypeAhead(userFilter: UserFilter): Observable<Array<User>>;
    deleteObj(id: any): Observable<OnesMessage>;
    getObj(id: any): Observable<User>;
    save(userSave: User): Observable<User>;
}
