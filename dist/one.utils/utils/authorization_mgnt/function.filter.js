"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var FunctionFilter = (function () {
    function FunctionFilter() {
    }
    return FunctionFilter;
}());
exports.FunctionFilter = FunctionFilter;
exports.KEY_NAME = 'weddyvn.title.function';
exports.EXPORT_FIELDS = ['name', 'functionKey', 'api', 'method', 'allowAll', 'onOrg', 'onObj', 'icon'];
exports.TEMPLATE_FIELDS = ['name', 'functionKey', 'api', 'method', 'allowAll', 'onOrg', 'onObj', 'icon'];
//# sourceMappingURL=function.filter.js.map