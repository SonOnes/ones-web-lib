import { HttpRequestService } from '../common/http.request.service';
import { Observable } from 'rxjs/Observable';
import { OnesPaging } from '../common/one.obj';
import { Utils } from '../common/utils';
import { Function } from './function';
import { FunctionFilter } from './function.filter';
export declare class FunctionService {
    private http;
    private utils;
    constructor(http: HttpRequestService, utils: Utils);
    getMaxFirst(): Observable<Array<Function>>;
    searchForTypeAhead(functionFilter: FunctionFilter): Observable<Array<Function>>;
    search(functionFilter: FunctionFilter): Observable<OnesPaging<Function>>;
    getObj(id: any): Observable<Function>;
    save(functionSave: Function): Observable<Function>;
    export(filter: FunctionFilter): Observable<Blob>;
}
