import { HttpRequestService } from '../common/http.request.service';
import { Observable } from 'rxjs/Observable';
import { OnesPaging } from '../common/paging.ones.obj';
import { OnesMessage } from '../common/ones.message';
import { Utils } from '../common/utils';
import { Organization } from './organization';
import { OrganizationFilter } from './organization.filter';
export declare class MyOrgsService {
    private http;
    private utils;
    constructor(http: HttpRequestService, utils: Utils);
    searchForTypeAhead(organizationFilter: OrganizationFilter): Observable<Array<Organization>>;
    search(organizationFilter: OrganizationFilter): Observable<OnesPaging<Organization>>;
    deleteObj(id: any): Observable<OnesMessage>;
    getObj(id: any): Observable<Organization>;
    save(organizationSave: Organization): Observable<Organization>;
    export(filter: OrganizationFilter): Observable<Blob>;
}
