"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_request_service_1 = require("../common/http.request.service");
var config_1 = require("../../../config");
var user_information_filter_1 = require("./user.information.filter");
var UserinformationService = (function () {
    function UserinformationService(http) {
        this.http = http;
    }
    UserinformationService.prototype.getFirst1000 = function () {
        var userInformationFilter = new user_information_filter_1.UserInformationFilter();
        userInformationFilter.limit = config_1.Config.MAX_RESULT;
        return this.search(userInformationFilter);
    };
    UserinformationService.prototype.search = function (userInformationFilter) {
        var userInformations = this.http.post(config_1.USERINFORMATION_GET_LIST.apiEndpoint, JSON.stringify(userInformationFilter))
            .map(function (res) { return res.json(); });
        return userInformations;
    };
    UserinformationService.prototype.deleteObj = function (id) {
        var query = '?';
        if (config_1.USERINFORMATION_DEL_OBJ.apiEndpoint.indexOf('?') > -1) {
            query = '&';
        }
        query += id;
        var message = this.http.delete(config_1.USERINFORMATION_DEL_OBJ.apiEndpoint + query)
            .map(function (res) { return res.json(); });
        return message;
    };
    UserinformationService.prototype.deleteObjs = function (id) {
        var query = '?';
        if (config_1.USERINFORMATION_DEL_OBJs.apiEndpoint.indexOf('?') > -1) {
            query = '&';
        }
        query += id;
        var message;
        return message;
    };
    UserinformationService.prototype.getObj = function (id) {
        var query = '?';
        if (config_1.USERINFORMATION_GET_DETAIL.apiEndpoint.indexOf('?') > -1) {
            query = '&';
        }
        query += id;
        var userInformation = this.http.get(config_1.USERINFORMATION_GET_DETAIL.apiEndpoint + query)
            .map(function (res) { return res.json(); });
        return userInformation;
    };
    UserinformationService.prototype.save = function (userInformationSave) {
        var userInformation = this.http.post(config_1.USERINFORMATION_EDIT_OBJ.apiEndpoint, JSON.stringify(userInformationSave))
            .map(function (res) { return res.json(); });
        return userInformation;
    };
    UserinformationService.decorators = [
        { type: core_1.Injectable },
    ];
    /** @nocollapse */
    UserinformationService.ctorParameters = function () { return [
        { type: http_request_service_1.HttpRequestService, },
    ]; };
    return UserinformationService;
}());
exports.UserinformationService = UserinformationService;
//# sourceMappingURL=userinformation.service.js.map