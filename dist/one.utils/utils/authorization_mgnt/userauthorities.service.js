"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_request_service_1 = require("../common/http.request.service");
var config_1 = require("../../../config");
var user_authorities_filter_1 = require("./user.authorities.filter");
var UserauthoritiesService = (function () {
    function UserauthoritiesService(http) {
        this.http = http;
    }
    UserauthoritiesService.prototype.getFirst1000 = function () {
        var userAuthoritiesFilter = new user_authorities_filter_1.UserAuthoritiesFilter();
        userAuthoritiesFilter.limit = config_1.Config.MAX_RESULT;
        return this.search(userAuthoritiesFilter);
    };
    UserauthoritiesService.prototype.search = function (userAuthoritiesFilter) {
        var userAuthoritiess = this.http.post(config_1.USERAUTHORITIES_GET_LIST.apiEndpoint, JSON.stringify(userAuthoritiesFilter))
            .map(function (res) { return res.json(); });
        return userAuthoritiess;
    };
    UserauthoritiesService.prototype.deleteObj = function (id) {
        var query = '?';
        if (config_1.USERAUTHORITIES_DEL_OBJ.apiEndpoint.indexOf('?') > -1) {
            query = '&';
        }
        query += id;
        var message = this.http.delete(config_1.USERAUTHORITIES_DEL_OBJ.apiEndpoint + query)
            .map(function (res) { return res.json(); });
        return message;
    };
    UserauthoritiesService.prototype.getObj = function (id) {
        var query = '?';
        if (config_1.USERAUTHORITIES_GET_DETAIL.apiEndpoint.indexOf('?') > -1) {
            query = '&';
        }
        query += id;
        var userAuthorities = this.http.get(config_1.USERAUTHORITIES_GET_DETAIL.apiEndpoint + query)
            .map(function (res) { return res.json(); });
        return userAuthorities;
    };
    UserauthoritiesService.prototype.save = function (userAuthoritiesSave) {
        var userAuthorities = this.http.post(config_1.USERAUTHORITIES_EDIT_OBJ.apiEndpoint, JSON.stringify(userAuthoritiesSave))
            .map(function (res) { return res.json(); });
        return userAuthorities;
    };
    UserauthoritiesService.decorators = [
        { type: core_1.Injectable },
    ];
    /** @nocollapse */
    UserauthoritiesService.ctorParameters = function () { return [
        { type: http_request_service_1.HttpRequestService, },
    ]; };
    return UserauthoritiesService;
}());
exports.UserauthoritiesService = UserauthoritiesService;
//# sourceMappingURL=userauthorities.service.js.map