export declare class Organization {
    id: number;
    code: string;
    name: string;
    createdFor: number;
    createdBy: number;
    createdOn: string;
    updatedBy: number;
    updatedOn: string;
    parent: number;
}
