import { HttpRequestService } from '../common/http.request.service';
import { Observable } from 'rxjs/Observable';
import { OnesPaging } from '../common/paging.ones.obj';
import { OnesMessage } from '../common/ones.message';
import { Authority } from './authority';
import { AuthorityFilter } from './authority.filter';
export declare class AuthorityService {
    private http;
    constructor(http: HttpRequestService);
    getFirst1000(): Observable<OnesPaging<Authority>>;
    search(authorityFilter: AuthorityFilter): Observable<OnesPaging<Authority>>;
    deleteObj(id: any): Observable<OnesMessage>;
    deleteObjs(id: any): Observable<OnesMessage>;
    getObj(id: any): Observable<Authority>;
    save(authoritySave: Authority): Observable<Authority>;
}
