"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_request_service_1 = require("../common/http.request.service");
var utils_1 = require("../common/utils");
var config_1 = require("../../../config");
var MyOrgsService = (function () {
    function MyOrgsService(http, utils) {
        this.http = http;
        this.utils = utils;
    }
    MyOrgsService.prototype.searchForTypeAhead = function (organizationFilter) {
        var organizations = this.http.post(config_1.MY_ORG_GET_LIST.apiEndpoint, JSON.stringify(organizationFilter))
            .map(function (res) {
            var jsonObj = res.json();
            return jsonObj.content;
        });
        return organizations;
    };
    MyOrgsService.prototype.search = function (organizationFilter) {
        var organizations = this.http.post(config_1.MY_ORG_GET_LIST.apiEndpoint, JSON.stringify(organizationFilter))
            .map(function (res) { return res.json(); });
        return organizations;
    };
    MyOrgsService.prototype.deleteObj = function (id) {
        var query = '?';
        if (config_1.MY_ORGS_DEL_OBJ.apiEndpoint.indexOf('?') > -1) {
            query = '&';
        }
        query += id;
        var message = this.http.delete(config_1.MY_ORGS_DEL_OBJ.apiEndpoint + query)
            .map(function (res) { return res.json(); });
        return message;
    };
    MyOrgsService.prototype.getObj = function (id) {
        var query = '?';
        if (config_1.MY_ORGS_GET_DETAIL.apiEndpoint.indexOf('?') > -1) {
            query = '&';
        }
        query += id;
        var organizationDetail = this.http.get(config_1.MY_ORGS_GET_DETAIL.apiEndpoint + query)
            .map(function (res) { return res.json(); });
        return organizationDetail;
    };
    MyOrgsService.prototype.save = function (organizationSave) {
        var organizationDetail = this.http.post(config_1.MY_ORGS_EDIT_OBJ.apiEndpoint, JSON.stringify(organizationSave))
            .map(function (res) { return res.json(); });
        return organizationDetail;
    };
    MyOrgsService.prototype.export = function (filter) {
        var res = this.http.postDownload(config_1.MY_ORGS_EXPORT.apiEndpoint, JSON.stringify(filter)).map(function (res) {
            return new Blob([res.blob()], { type: config_1.Config.importTemplateType });
        });
        return res;
    };
    MyOrgsService.decorators = [
        { type: core_1.Injectable },
    ];
    /** @nocollapse */
    MyOrgsService.ctorParameters = function () { return [
        { type: http_request_service_1.HttpRequestService, },
        { type: utils_1.Utils, },
    ]; };
    return MyOrgsService;
}());
exports.MyOrgsService = MyOrgsService;
//# sourceMappingURL=my.orgs.service.js.map