import { OnesSort } from '../common/one.obj';
export declare class FunctionFilter {
    id: number;
    name: string;
    functionKey: string;
    api: string;
    method: string;
    allowAll: boolean;
    onOrg: boolean;
    onObj: boolean;
    icon: string;
    parent: number;
    page: number;
    limit: number;
    options: any[];
    sortDTOs: OnesSort[];
}
export declare const KEY_NAME: string;
export declare const EXPORT_FIELDS: string[];
export declare const TEMPLATE_FIELDS: string[];
