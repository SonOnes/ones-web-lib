"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_request_service_1 = require("../common/http.request.service");
var utils_1 = require("../common/utils");
var config_1 = require("../../../config");
var MyProfileService = (function () {
    function MyProfileService(http, utils) {
        this.http = http;
        this.utils = utils;
    }
    MyProfileService.prototype.getObj = function () {
        var organizationDetail = this.http.get(config_1.LOGGING_USER.apiEndpoint)
            .map(function (res) { return res.json(); });
        return organizationDetail;
    };
    MyProfileService.prototype.save = function (organizationSave) {
        var organizationDetail = this.http.post(config_1.USER_EDIT_OBJ.apiEndpoint, JSON.stringify(organizationSave))
            .map(function (res) { return res.json(); });
        return organizationDetail;
    };
    MyProfileService.decorators = [
        { type: core_1.Injectable },
    ];
    /** @nocollapse */
    MyProfileService.ctorParameters = function () { return [
        { type: http_request_service_1.HttpRequestService, },
        { type: utils_1.Utils, },
    ]; };
    return MyProfileService;
}());
exports.MyProfileService = MyProfileService;
//# sourceMappingURL=my.profile.service.js.map