export declare class User {
    id: number;
    firstName: string;
    lastName: string;
    username: string;
    password: string;
    createdOn: string;
    currentLogin: string;
    lastLogin: string;
    private status;
    countryId: number;
    provinceId: number;
    provinceName: number;
    updatedOn: string;
    updatedBy: number;
    tel1: string;
    address: string;
    nickName: string;
    faceAccount: string;
    linkedAccount: string;
    githubAccount: string;
}
