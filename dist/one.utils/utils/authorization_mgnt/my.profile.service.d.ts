import { HttpRequestService } from '../common/http.request.service';
import { Observable } from 'rxjs/Observable';
import { Utils } from '../common/utils';
import { Organization } from './organization';
export declare class MyProfileService {
    private http;
    private utils;
    constructor(http: HttpRequestService, utils: Utils);
    getObj(): Observable<Organization>;
    save(organizationSave: Organization): Observable<Organization>;
}
