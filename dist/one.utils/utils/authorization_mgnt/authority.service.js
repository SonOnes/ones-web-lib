"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_request_service_1 = require("../common/http.request.service");
var config_1 = require("../../../config");
var authority_filter_1 = require("./authority.filter");
var AuthorityService = (function () {
    function AuthorityService(http) {
        this.http = http;
    }
    AuthorityService.prototype.getFirst1000 = function () {
        var authorityFilter = new authority_filter_1.AuthorityFilter();
        authorityFilter.limit = config_1.Config.MAX_RESULT;
        return this.search(authorityFilter);
    };
    AuthorityService.prototype.search = function (authorityFilter) {
        var authoritys = this.http.post(config_1.AUTHORITY_GET_LIST.apiEndpoint, JSON.stringify(authorityFilter))
            .map(function (res) { return res.json(); });
        return authoritys;
    };
    AuthorityService.prototype.deleteObj = function (id) {
        var query = '?';
        if (config_1.AUTHORITY_DEL_OBJ.apiEndpoint.indexOf('?') > -1) {
            query = '&';
        }
        query += id;
        var message = this.http.delete(config_1.AUTHORITY_DEL_OBJ.apiEndpoint + query)
            .map(function (res) { return res.json(); });
        return message;
    };
    AuthorityService.prototype.deleteObjs = function (id) {
        var query = '?';
        if (config_1.AUTHORITY_DEL_OBJs.apiEndpoint.indexOf('?') > -1) {
            query = '&';
        }
        query += id;
        var message;
        return message;
    };
    AuthorityService.prototype.getObj = function (id) {
        var query = '?';
        if (config_1.AUTHORITY_GET_DETAIL.apiEndpoint.indexOf('?') > -1) {
            query = '&';
        }
        query += id;
        var authority = this.http.get(config_1.AUTHORITY_GET_DETAIL.apiEndpoint + query)
            .map(function (res) { return res.json(); });
        return authority;
    };
    AuthorityService.prototype.save = function (authoritySave) {
        var authority = this.http.post(config_1.AUTHORITY_EDIT_OBJ.apiEndpoint, JSON.stringify(authoritySave))
            .map(function (res) { return res.json(); });
        return authority;
    };
    AuthorityService.decorators = [
        { type: core_1.Injectable },
    ];
    /** @nocollapse */
    AuthorityService.ctorParameters = function () { return [
        { type: http_request_service_1.HttpRequestService, },
    ]; };
    return AuthorityService;
}());
exports.AuthorityService = AuthorityService;
//# sourceMappingURL=authority.service.js.map