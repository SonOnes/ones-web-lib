export declare class UserInformation {
    id: number;
    userId: number;
    verifyToken: string;
    verifiedOn: string;
}
