import { OnesSort } from '../common/ones.sort.obj';
export declare class OrganizationFilter {
    id: number;
    code: string;
    name: string;
    createdFor: number;
    createdBy: number;
    createdOnFrom: string;
    createdOnTo: string;
    updatedBy: number;
    updatedOnFrom: string;
    updatedOnTo: string;
    parent: number;
    page: number;
    limit: number;
    options: any[];
    sortDTOs: OnesSort[];
}
export declare const KEY_NAME: string;
export declare const EXPORT_FIELDS: string[];
export declare const TEMPLATE_FIELDS: string[];
