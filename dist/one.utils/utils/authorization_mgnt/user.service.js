"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_request_service_1 = require("../common/http.request.service");
var config_1 = require("../../../config");
var user_filter_1 = require("./user.filter");
var UserService = (function () {
    function UserService(http) {
        this.http = http;
    }
    UserService.prototype.getFirst1000 = function () {
        var userFilter = new user_filter_1.UserFilter();
        userFilter.limit = config_1.Config.MAX_RESULT;
        return this.search(userFilter);
    };
    UserService.prototype.search = function (userFilter) {
        var users = this.http.post(config_1.USER_GET_LIST.apiEndpoint, JSON.stringify(userFilter))
            .map(function (res) { return res.json(); });
        return users;
    };
    UserService.prototype.searchForTypeAhead = function (userFilter) {
        var organizations = this.http.post(config_1.USER_GET_LIST.apiEndpoint, JSON.stringify(userFilter))
            .map(function (res) {
            var jsonObj = res.json();
            return jsonObj.content;
        });
        return organizations;
    };
    UserService.prototype.deleteObj = function (id) {
        var query = '?';
        if (config_1.USER_DEL_OBJ.apiEndpoint.indexOf('?') > -1) {
            query = '&';
        }
        query += id;
        var message = this.http.delete(config_1.USER_DEL_OBJ.apiEndpoint + query)
            .map(function (res) { return res.json(); });
        return message;
    };
    UserService.prototype.getObj = function (id) {
        var query = '?';
        if (config_1.USER_GET_DETAIL.apiEndpoint.indexOf('?') > -1) {
            query = '&';
        }
        query += id;
        var user = this.http.get(config_1.USER_GET_DETAIL.apiEndpoint + query)
            .map(function (res) { return res.json(); });
        return user;
    };
    UserService.prototype.save = function (userSave) {
        var user = this.http.post(config_1.USER_EDIT_OBJ.apiEndpoint, JSON.stringify(userSave))
            .map(function (res) { return res.json(); });
        return user;
    };
    UserService.decorators = [
        { type: core_1.Injectable },
    ];
    /** @nocollapse */
    UserService.ctorParameters = function () { return [
        { type: http_request_service_1.HttpRequestService, },
    ]; };
    return UserService;
}());
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map