import { HttpRequestService } from '../common/http.request.service';
import { Observable } from 'rxjs/Observable';
import { OnesPaging } from '../common/paging.ones.obj';
import { OnesMessage } from '../common/ones.message';
import { UserAuthorities } from './user.authorities';
import { UserAuthoritiesFilter } from './user.authorities.filter';
export declare class UserauthoritiesService {
    private http;
    constructor(http: HttpRequestService);
    getFirst1000(): Observable<OnesPaging<UserAuthorities>>;
    search(userAuthoritiesFilter: UserAuthoritiesFilter): Observable<OnesPaging<UserAuthorities>>;
    deleteObj(id: any): Observable<OnesMessage>;
    getObj(id: any): Observable<UserAuthorities>;
    save(userAuthoritiesSave: UserAuthorities): Observable<UserAuthorities>;
}
