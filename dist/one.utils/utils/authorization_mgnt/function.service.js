"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_request_service_1 = require("../common/http.request.service");
var utils_1 = require("../common/utils");
var config_1 = require("../../../config");
var function_filter_1 = require("./function.filter");
var FunctionService = (function () {
    function FunctionService(http, utils) {
        this.http = http;
        this.utils = utils;
    }
    FunctionService.prototype.getMaxFirst = function () {
        var functionFilter = new function_filter_1.FunctionFilter();
        functionFilter.limit = config_1.Config.MAX_RESULT;
        var functions = this.http.post(config_1.FUNCTION_GET_LIST.apiEndpoint, JSON.stringify(functionFilter))
            .map(function (res) {
            var jsonObj = res.json();
            return jsonObj.content;
        });
        return functions;
    };
    FunctionService.prototype.searchForTypeAhead = function (functionFilter) {
        var functions = this.http.post(config_1.FUNCTION_GET_LIST.apiEndpoint, JSON.stringify(functionFilter))
            .map(function (res) {
            var jsonObj = res.json();
            return jsonObj.content;
        });
        return functions;
    };
    FunctionService.prototype.search = function (functionFilter) {
        var functions = this.http.post(config_1.FUNCTION_GET_LIST.apiEndpoint, JSON.stringify(functionFilter))
            .map(function (res) { return res.json(); });
        return functions;
    };
    FunctionService.prototype.getObj = function (id) {
        var query = '?';
        if (config_1.FUNCTION_GET_DETAIL.apiEndpoint.indexOf('?') > -1) {
            query = '&';
        }
        query += id;
        var functionDetail = this.http.get(config_1.FUNCTION_GET_DETAIL.apiEndpoint + query)
            .map(function (res) { return res.json(); });
        return functionDetail;
    };
    FunctionService.prototype.save = function (functionSave) {
        var functionDetail = this.http.post(config_1.FUNCTION_EDIT_OBJ.apiEndpoint, JSON.stringify(functionSave))
            .map(function (res) { return res.json(); });
        return functionDetail;
    };
    FunctionService.prototype.export = function (filter) {
        var res = this.http.postDownload(config_1.FUNCTION_EXPORT.apiEndpoint, JSON.stringify(filter)).map(function (res) {
            return new Blob([res.blob()], { type: config_1.Config.importTemplateType });
        });
        return res;
    };
    FunctionService.decorators = [
        { type: core_1.Injectable },
    ];
    /** @nocollapse */
    FunctionService.ctorParameters = function () { return [
        { type: http_request_service_1.HttpRequestService, },
        { type: utils_1.Utils, },
    ]; };
    return FunctionService;
}());
exports.FunctionService = FunctionService;
//# sourceMappingURL=function.service.js.map