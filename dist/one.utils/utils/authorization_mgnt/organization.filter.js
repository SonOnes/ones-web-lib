"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var OrganizationFilter = (function () {
    function OrganizationFilter() {
    }
    return OrganizationFilter;
}());
exports.OrganizationFilter = OrganizationFilter;
exports.KEY_NAME = 'onesuser.title.organization';
exports.EXPORT_FIELDS = ['code', 'name', 'createdFor', 'createdOn', 'updatedOn', 'parent'];
exports.TEMPLATE_FIELDS = ['code', 'name', 'createdFor', 'parent'];
//# sourceMappingURL=organization.filter.js.map