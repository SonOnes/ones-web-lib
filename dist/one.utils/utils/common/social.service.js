"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var angular2_social_login_1 = require("angular2-social-login");
var SocialService = (function () {
    function SocialService(_auth) {
        this._auth = _auth;
    }
    SocialService.prototype.signIn = function (provider) {
        console.log('---------------' + provider);
        this._auth.login(provider).subscribe(function (data) {
            console.log(data);
            //user data
            //name, image, uid, provider, uid, email, token (accessToken for Facebook & google, no token for linkedIn), idToken(only for google)
        });
    };
    SocialService.prototype.logout = function () {
        this._auth.logout().subscribe(function (data) {
            //return a boolean value.
        });
    };
    SocialService.decorators = [
        { type: core_1.Injectable },
    ];
    /** @nocollapse */
    SocialService.ctorParameters = function () { return [
        { type: angular2_social_login_1.AuthService, },
    ]; };
    return SocialService;
}());
exports.SocialService = SocialService;
//# sourceMappingURL=social.service.js.map