"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var ng_bootstrap_1 = require("@ng-bootstrap/ng-bootstrap");
var NgbDateObjParserFormatter = (function (_super) {
    __extends(NgbDateObjParserFormatter, _super);
    function NgbDateObjParserFormatter(momentFormat) {
        var _this = _super.call(this) || this;
        _this.momentFormat = momentFormat;
        return _this;
    }
    ;
    NgbDateObjParserFormatter.prototype.format = function (date) {
        if (date === null) {
            return '';
        }
        return ('0' + date.day + '').slice(-2) + '-' + ('0' + date.month + '').slice(-2) + '-' + date.year + '';
    };
    NgbDateObjParserFormatter.prototype.parse = function (value) {
        if (value) {
            console.log(value);
            var dateParts = value.trim().split('-');
            if (dateParts.length === 1 && isNumber(dateParts[0])) {
                return { year: toInteger(dateParts[0]), month: null, day: null };
            }
            else if (dateParts.length === 2 && isNumber(dateParts[0]) && isNumber(dateParts[1])) {
                return { year: toInteger(dateParts[1]), month: toInteger(dateParts[0]), day: null };
            }
            else if (dateParts.length === 3 && isNumber(dateParts[0]) && isNumber(dateParts[1]) && isNumber(dateParts[2])) {
                return { year: toInteger(dateParts[2]), month: toInteger(dateParts[1]), day: toInteger(dateParts[0]) };
            }
        }
        return null;
    };
    return NgbDateObjParserFormatter;
}(ng_bootstrap_1.NgbDateParserFormatter));
exports.NgbDateObjParserFormatter = NgbDateObjParserFormatter;
function toInteger(value) {
    return parseInt('' + value, 10);
}
exports.toInteger = toInteger;
function toString(value) {
    return (value !== undefined && value !== null) ? '' + value : '';
}
exports.toString = toString;
function getValueInRange(value, max, min) {
    if (min === void 0) {
        min = 0;
    }
    return Math.max(Math.min(value, max), min);
}
exports.getValueInRange = getValueInRange;
function isString(value) {
    return typeof value === 'string';
}
exports.isString = isString;
function isNumber(value) {
    return !isNaN(toInteger(value));
}
exports.isNumber = isNumber;
function isDefined(value) {
    return value !== undefined && value !== null;
}
exports.isDefined = isDefined;
function padNumber(value) {
    if (isNumber(value)) {
        return ('0' + value).slice(-2);
    }
    else {
        return '';
    }
}
exports.padNumber = padNumber;
//# sourceMappingURL=ngbdatepicker.formatter.js.map