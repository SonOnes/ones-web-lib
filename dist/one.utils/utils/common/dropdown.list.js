"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var dropdown_list_item_1 = require("./dropdown.list.item");
var DropdownList = (function () {
    function DropdownList(strEnum, objs) {
        this.items = [];
        if (null != strEnum && 0 < strEnum.length) {
            for (var _i = 0, strEnum_1 = strEnum; _i < strEnum_1.length; _i++) {
                var str = strEnum_1[_i];
                this.items.push(new dropdown_list_item_1.DropdownListItem(str, str));
            }
            return;
        }
        if (null != objs && 0 < objs.length) {
            for (var _a = 0, objs_1 = objs; _a < objs_1.length; _a++) {
                var obj = objs_1[_a];
                this.items.push(new dropdown_list_item_1.DropdownListItem(obj.id, obj.name));
            }
        }
    }
    DropdownList.prototype.toValue = function (items) {
        var results = [];
        if (null == items) {
            return results;
        }
        for (var _i = 0, items_1 = items; _i < items_1.length; _i++) {
            var item = items_1[_i];
            results.push(item.id);
        }
        return results;
    };
    return DropdownList;
}());
exports.DropdownList = DropdownList;
//# sourceMappingURL=dropdown.list.js.map