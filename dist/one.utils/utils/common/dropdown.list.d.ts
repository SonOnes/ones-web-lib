import { DropdownListItem } from './dropdown.list.item';
export declare class DropdownList {
    items: DropdownListItem[];
    constructor(strEnum: string[], objs: any[]);
    toValue(items: DropdownListItem[]): any[];
}
