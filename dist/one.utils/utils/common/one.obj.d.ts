import { TranslateService } from '@ngx-translate/core';
export declare class AppUser {
    id: string;
    username: string;
    fullName: string;
    roles: string[];
    apiFunctions: any[];
    menuFunctions: any[];
}
export declare class DropdownListItem {
    id: any;
    itemName: String;
    constructor(id: any, itemName: string);
}
export declare class DropdownList {
    items: DropdownListItem[];
    constructor(strEnum: string[], objs: any[]);
    toValue(items: DropdownListItem[]): any[];
}
export declare class Dropdown {
    itemsSelected: DropdownListItem[];
    listItem: DropdownListItem[];
    settings: {
        singleSelection: boolean;
        text: string;
        selectAllText: string;
        unSelectAllText: string;
        enableSearchFilter: boolean;
        classes: string;
    };
    constructor();
}
export declare class FieldObj {
    fieldName: string;
    fieldTitle: string;
}
export declare class GridColumn {
    colId: string;
    headerName: string;
    width: number;
    checkboxSelection: boolean;
    suppressSorting: boolean;
    suppressMenu: boolean;
    pinned: any;
    template: string;
    field: string;
    valueFormatter: any;
    static key: string;
    static translate: TranslateService;
    constructor(id: string, field: string, width: number, checkboxSelection: boolean, suppressSorting: boolean, suppressMenu: boolean, pinned: any, template: string, valueFormatter: any);
}
export declare class OnesSort {
    property: string;
    direction: string;
}
export declare class OnesMessage {
    code: string;
    message: string;
    type: string;
    violations: any[];
}
export declare class OnesPaging<T> {
    first: boolean;
    last: boolean;
    number: number;
    numberOfElements: number;
    size: number;
    totalElements: number;
    totalPages: number;
    content: T[];
    constructor(data: any);
}
export declare class Typeahead {
    searching: boolean;
    searchNoData: boolean;
    inputSuggestFormatter: any;
    formatterSuggestResult: any;
    suggestion: any;
    model: any;
}
