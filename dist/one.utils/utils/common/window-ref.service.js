"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
function getWindow() {
    return window;
}
exports.getWindow = getWindow;
var WindowRefService = (function () {
    function WindowRefService() {
    }
    Object.defineProperty(WindowRefService.prototype, "nativeWindow", {
        get: function () {
            return getWindow();
        },
        enumerable: true,
        configurable: true
    });
    WindowRefService.decorators = [
        { type: core_1.Injectable },
    ];
    /** @nocollapse */
    WindowRefService.ctorParameters = function () { return []; };
    return WindowRefService;
}());
exports.WindowRefService = WindowRefService;
//# sourceMappingURL=window-ref.service.js.map