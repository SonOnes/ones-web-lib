"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GridColumn = (function () {
    function GridColumn(id, field, width, checkboxSelection, suppressSorting, suppressMenu, pinned, template, valueFormatter) {
        var columnName = '';
        if (null != field) {
            GridColumn.translate.get(GridColumn.key + field).subscribe(function (data) {
                columnName = data;
            });
        }
        this.colId = id;
        this.checkboxSelection = checkboxSelection;
        this.headerName = columnName;
        this.field = field;
        this.pinned = pinned;
        this.suppressMenu = suppressMenu;
        this.suppressSorting = suppressSorting;
        this.template = template;
        this.width = width;
        this.valueFormatter = valueFormatter;
    }
    return GridColumn;
}());
exports.GridColumn = GridColumn;
//# sourceMappingURL=grid.column.js.map