import { Router } from '@angular/router';
import { LocalStorageService } from '../../localforage/local-storage.service';
import { HttpRequestService } from '../common/http.request.service';
import { Observable } from 'rxjs/Observable';
import { AppUser } from './one.obj';
export declare class OneUserService {
    private http;
    private router;
    storageService: LocalStorageService;
    constructor(http: HttpRequestService, router: Router, storageService: LocalStorageService);
    getLogginUser(): Observable<AppUser>;
    checkRoles(key: string, user: AppUser): boolean;
    checkAuthority(key: string, method: string, user: AppUser): boolean;
    login(username: string, pass: string): Observable<any>;
    logout(loginUrl: string): void;
}
