import { DropdownListItem } from './dropdown.list.item';
export declare class Dropdown {
    itemsSelected: DropdownListItem[];
    listItem: DropdownListItem[];
    settings: {
        singleSelection: boolean;
        text: string;
        selectAllText: string;
        unSelectAllText: string;
        enableSearchFilter: boolean;
        classes: string;
    };
    constructor();
}
