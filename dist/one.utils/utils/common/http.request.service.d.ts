import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Http, ConnectionBackend, RequestOptions, RequestOptionsArgs, Request, Response, ResponseContentType } from '@angular/http';
export declare class HttpRequestService extends Http {
    private _router;
    private storageService;
    constructor(backend: ConnectionBackend, defaultOptions: RequestOptions, _router: Router);
    request(url: string | Request, options?: RequestOptionsArgs): Observable<Response>;
    get(url: string, options?: RequestOptionsArgs): Observable<Response>;
    post(url: string, body: string, options?: RequestOptionsArgs): Observable<Response>;
    put(url: string, body: string, options?: RequestOptionsArgs): Observable<Response>;
    delete(url: string, options?: RequestOptionsArgs): Observable<Response>;
    getDownload(url: string, options?: RequestOptionsArgs): Observable<Response>;
    postDownload(url: string, body: string, options?: RequestOptionsArgs): Observable<Response>;
    postUpload(url: string, file: any, options?: RequestOptionsArgs): Observable<Response>;
    getRequestOptionArgs(options?: RequestOptionsArgs): RequestOptionsArgs;
    getDownloadRequestOptionArgs(options?: RequestOptionsArgs): RequestOptionsArgs;
    postUploadRequestOptionArgs(options?: RequestOptionsArgs): RequestOptionsArgs;
    getUploadRequestOptionArgs(resType: ResponseContentType, reqContentType: string): RequestOptionsArgs;
    private fullPath(path);
    private intercept(observable);
}
