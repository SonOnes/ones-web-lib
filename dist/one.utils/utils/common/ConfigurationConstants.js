"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ConfigurationConstants = (function () {
    function ConfigurationConstants() {
        this._cookieName = 'etl_language';
    }
    ConfigurationConstants.prototype.getCookieName = function () {
        return this._cookieName;
    };
    ConfigurationConstants.decorators = [
        { type: core_1.Injectable },
    ];
    /** @nocollapse */
    ConfigurationConstants.ctorParameters = function () { return []; };
    return ConfigurationConstants;
}());
exports.ConfigurationConstants = ConfigurationConstants;
/*
export var CONSTANTS_PROVIDERS:Provider[] = [
  provide(ConfigurationConstants, {useClass: ConfigurationConstants})
];*/
//# sourceMappingURL=ConfigurationConstants.js.map