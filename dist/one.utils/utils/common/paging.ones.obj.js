"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var OnesPaging = (function () {
    function OnesPaging(data) {
        this.first = data.first;
        this.last = data.last;
        this.number = data.number;
        this.numberOfElements = data.numberOfElements;
        this.size = data.size;
        this.totalElements = data.totalElements;
        this.totalPages = data.totalPages;
        this.content = data.content;
    }
    return OnesPaging;
}());
exports.OnesPaging = OnesPaging;
//# sourceMappingURL=paging.ones.obj.js.map