import { NgbDateStruct, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
export declare class NgbDateObjParserFormatter extends NgbDateParserFormatter {
    private momentFormat;
    constructor(momentFormat: string);
    format(date: NgbDateStruct): string;
    parse(value: string): NgbDateStruct;
}
export declare function toInteger(value: any): number;
export declare function toString(value: any): string;
export declare function getValueInRange(value: any, max: any, min: any): number;
export declare function isString(value: any): boolean;
export declare function isNumber(value: any): boolean;
export declare function isDefined(value: any): boolean;
export declare function padNumber(value: any): string;
