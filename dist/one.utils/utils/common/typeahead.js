"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Typeahead = (function () {
    function Typeahead() {
        this.searching = false;
        this.searchNoData = false;
    }
    return Typeahead;
}());
exports.Typeahead = Typeahead;
//# sourceMappingURL=typeahead.js.map