import { TranslateService } from '@ngx-translate/core';
export declare class GridColumn {
    colId: string;
    headerName: string;
    width: number;
    checkboxSelection: boolean;
    suppressSorting: boolean;
    suppressMenu: boolean;
    pinned: any;
    template: string;
    field: string;
    valueFormatter: any;
    static key: string;
    static translate: TranslateService;
    constructor(id: string, field: string, width: number, checkboxSelection: boolean, suppressSorting: boolean, suppressMenu: boolean, pinned: any, template: string, valueFormatter: any);
}
