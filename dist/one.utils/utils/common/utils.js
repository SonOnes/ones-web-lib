"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var ones_message_1 = require("./ones.message");
var config_1 = require("../../../config");
var field_obj_1 = require("./field.obj");
var angular2_uuid_1 = require("angular2-uuid");
var Utils = (function () {
    function Utils() {
    }
    Utils.prototype.isValid = function (obj) {
        return obj !== null && obj !== undefined;
    };
    Utils.prototype.handleError = function (error, alerts) {
        var message = new ones_message_1.OnesMessage();
        message.type = 'danger';
        alerts = [];
        if (error instanceof http_1.Response) {
            var body = error.json() || '';
            var err = body.error || JSON.stringify(body);
            message.code = body.code;
            message.message = body.message;
            message.violations = body.violations;
            if (401 === error.status) {
                window.location.href = config_1.Config.rootService + body.message;
            }
            else if (0 === error.status) {
                message.message = 'Cannot connect to server';
            }
        }
        alerts.push({ msg: message.message, type: message.type, closable: true, violations: message.violations });
        return alerts;
    };
    Utils.prototype.handleSuccess = function (success, alerts) {
        var message = new ones_message_1.OnesMessage();
        message.type = 'success';
        message.message = success.code;
        alerts = [];
        alerts.push({ msg: message.message, type: message.type, closable: true });
        return alerts;
    };
    Utils.prototype.getSorts = function (sorts) {
        var sortDTOs = [];
        if (undefined !== sorts && undefined !== sorts[0]) {
            var sortDirection = 'ASC';
            if (sorts[0].sort !== 'asc') {
                sortDirection = 'DESC';
            }
            else {
                sortDirection = 'ASC';
            }
            sortDTOs.push({ property: sorts[0].colId, direction: sortDirection });
        }
        return sortDTOs;
    };
    Utils.prototype.generateHeader = function () {
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        return headers;
    };
    Utils.prototype.initConfigurationToastMessage = function (message, title, navigationLinkAfterMessageRemoved, router) {
        var toastOptions = {
            showClose: true,
            timeout: config_1.Config.TOAST_MESSAGE_TIMEOUT,
            theme: config_1.Config.toastTheme,
            msg: message,
            title: title,
            onRemove: function (toast) {
                if (navigationLinkAfterMessageRemoved !== '') {
                    router.navigate([navigationLinkAfterMessageRemoved]);
                }
            }
        };
        return toastOptions;
    };
    Utils.prototype.parseTsDate = function (date) {
        if (null === date || undefined === date) {
            return null;
        }
        var tmp = new Date(date);
        var dateModel = {
            'year': tmp.getUTCFullYear(),
            'month': tmp.getMonth() + 1,
            'day': tmp.getDate()
        };
        return dateModel;
    };
    Utils.prototype.parseJVDate = function (date) {
        if (null === date || undefined === date) {
            return null;
        }
        if ((date.day.toString().length > 2 || date.day < 0 || date.day > 31)) {
            return null;
        }
        if ((date.month.toString().length > 2 || date.month < 0 || date.month > 12)) {
            return null;
        }
        if ((date.year.toString().length !== 4 || date.year < 0)) {
            return null;
        }
        return date.year + '-' + date.month + '-' + date.day;
    };
    Utils.prototype.deepCopy = function (oldObj) {
        var newObj = oldObj;
        if (oldObj && (typeof oldObj === 'object')) {
            newObj = Object.prototype.toString.call(oldObj) === '[object Array]' ? [] : {};
            for (var i in oldObj) {
                if (oldObj.hasOwnProperty(i)) {
                    newObj[i] = this.deepCopy(this.isValid(oldObj[i]) ? oldObj[i].valueOf() : oldObj[i]);
                }
            }
        }
        return newObj;
    };
    Utils.prototype.downloadFile = function (data) {
        var url = window.URL.createObjectURL(data);
        window.open(url);
    };
    Utils.prototype.downloadFileLink = function (data) {
        var url = window.URL.createObjectURL(data);
        var link = document.createElement('a');
        link.href = url;
        link.download = angular2_uuid_1.UUID.UUID();
        link.click();
    };
    Utils.prototype.translate = function (translate, key, strs) {
        if (null == strs || 0 === strs.length) {
            return null;
        }
        var tmpKey = [];
        for (var str in strs) {
            tmpKey.push(key + strs[str]);
        }
        return translate.get(tmpKey);
    };
    Utils.prototype.buildFieldOption = function (key, strs, trans) {
        var result = [];
        for (var i in strs) {
            var field = new field_obj_1.FieldObj();
            field.fieldName = strs[i];
            var tmp = key + strs[i];
            for (var j in trans) {
                if (tmp === j) {
                    field.fieldTitle = trans[j];
                }
            }
            result.push(field);
        }
        return result;
    };
    Utils.prototype.parseSelectValues = function (items) {
        var results = [];
        if (null == items) {
            return results;
        }
        for (var _i = 0, items_1 = items; _i < items_1.length; _i++) {
            var item = items_1[_i];
            results.push(item.id);
        }
        return results;
    };
    Utils.prototype.selectValue = function (id, items) {
        var results = [];
        if (null == items || null === id) {
            return results;
        }
        for (var _i = 0, items_2 = items; _i < items_2.length; _i++) {
            var item = items_2[_i];
            if (id === item.id) {
                results.push(item);
            }
        }
        return results;
    };
    Utils.prototype.selectValues = function (items) {
        var results = [];
        if (null == items) {
            return results;
        }
        for (var _i = 0, items_3 = items; _i < items_3.length; _i++) {
            var item = items_3[_i];
            results.push(item.id);
        }
        return results;
    };
    Utils.decorators = [
        { type: core_1.Injectable },
    ];
    /** @nocollapse */
    Utils.ctorParameters = function () { return []; };
    return Utils;
}());
exports.Utils = Utils;
//# sourceMappingURL=utils.js.map