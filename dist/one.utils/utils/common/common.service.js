"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var CommonService = (function () {
    function CommonService() {
    }
    CommonService.decorators = [
        { type: core_1.Injectable },
    ];
    /** @nocollapse */
    CommonService.ctorParameters = function () { return []; };
    return CommonService;
}());
exports.CommonService = CommonService;
//# sourceMappingURL=common.service.js.map