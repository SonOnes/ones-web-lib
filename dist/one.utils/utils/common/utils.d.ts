import { Headers } from '@angular/http';
import { OnesMessage } from './ones.message';
import { OnesSort } from './ones.sort.obj';
import { DropdownListItem } from './dropdown.list.item';
import { FieldObj } from './field.obj';
import { ToastOptions } from 'ng2-toasty';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
export declare class Utils {
    isValid(obj: any): boolean;
    handleError(error: any, alerts: Array<Object>): Array<Object>;
    handleSuccess(success: OnesMessage, alerts: Array<Object>): Array<Object>;
    getSorts(sorts: any[]): OnesSort[];
    generateHeader(): Headers;
    initConfigurationToastMessage(message: string, title: string, navigationLinkAfterMessageRemoved: string, router: Router): ToastOptions;
    parseTsDate(date: any): any;
    parseJVDate(date: any): any;
    deepCopy(oldObj: any): any;
    downloadFile(data: Blob): void;
    downloadFileLink(data: Blob): void;
    translate(translate: TranslateService, key: string, strs: string[]): Observable<Array<any>>;
    buildFieldOption(key: string, strs: string[], trans: any[]): Array<FieldObj>;
    parseSelectValues(items: DropdownListItem[]): any[];
    selectValue(id: any, items: DropdownListItem[]): any[];
    selectValues(items: DropdownListItem[]): any[];
}
