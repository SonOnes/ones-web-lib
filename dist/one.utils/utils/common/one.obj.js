"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var config_1 = require("../../../config");
var AppUser = (function () {
    function AppUser() {
    }
    return AppUser;
}());
exports.AppUser = AppUser;
var DropdownListItem = (function () {
    function DropdownListItem(id, itemName) {
        this.id = id;
        this.itemName = itemName;
    }
    return DropdownListItem;
}());
exports.DropdownListItem = DropdownListItem;
var DropdownList = (function () {
    function DropdownList(strEnum, objs) {
        this.items = [];
        if (null != strEnum && 0 < strEnum.length) {
            for (var _i = 0, strEnum_1 = strEnum; _i < strEnum_1.length; _i++) {
                var str = strEnum_1[_i];
                this.items.push(new DropdownListItem(str, str));
            }
            return;
        }
        if (null != objs && 0 < objs.length) {
            for (var _a = 0, objs_1 = objs; _a < objs_1.length; _a++) {
                var obj = objs_1[_a];
                this.items.push(new DropdownListItem(obj.id, obj.name));
            }
        }
    }
    DropdownList.prototype.toValue = function (items) {
        var results = [];
        if (null == items) {
            return results;
        }
        for (var _i = 0, items_1 = items; _i < items_1.length; _i++) {
            var item = items_1[_i];
            results.push(item.id);
        }
        return results;
    };
    return DropdownList;
}());
exports.DropdownList = DropdownList;
var Dropdown = (function () {
    function Dropdown() {
        this.itemsSelected = [];
        this.listItem = [];
        this.settings = config_1.Config.multiDropdownSettings;
        this.itemsSelected = [];
        this.listItem = [];
        this.settings = config_1.Config.multiDropdownSettings;
    }
    return Dropdown;
}());
exports.Dropdown = Dropdown;
var FieldObj = (function () {
    function FieldObj() {
    }
    return FieldObj;
}());
exports.FieldObj = FieldObj;
var GridColumn = (function () {
    function GridColumn(id, field, width, checkboxSelection, suppressSorting, suppressMenu, pinned, template, valueFormatter) {
        var columnName = '';
        if (null != field) {
            GridColumn.translate.get(GridColumn.key + field).subscribe(function (data) {
                columnName = data;
            });
        }
        this.colId = id;
        this.checkboxSelection = checkboxSelection;
        this.headerName = columnName;
        this.field = field;
        this.pinned = pinned;
        this.suppressMenu = suppressMenu;
        this.suppressSorting = suppressSorting;
        this.template = template;
        this.width = width;
        this.valueFormatter = valueFormatter;
    }
    return GridColumn;
}());
exports.GridColumn = GridColumn;
var OnesSort = (function () {
    function OnesSort() {
    }
    return OnesSort;
}());
exports.OnesSort = OnesSort;
var OnesMessage = (function () {
    function OnesMessage() {
    }
    return OnesMessage;
}());
exports.OnesMessage = OnesMessage;
var OnesPaging = (function () {
    function OnesPaging(data) {
        this.first = data.first;
        this.last = data.last;
        this.number = data.number;
        this.numberOfElements = data.numberOfElements;
        this.size = data.size;
        this.totalElements = data.totalElements;
        this.totalPages = data.totalPages;
        this.content = data.content;
    }
    return OnesPaging;
}());
exports.OnesPaging = OnesPaging;
var Typeahead = (function () {
    function Typeahead() {
        this.searching = false;
        this.searchNoData = false;
    }
    return Typeahead;
}());
exports.Typeahead = Typeahead;
//# sourceMappingURL=one.obj.js.map