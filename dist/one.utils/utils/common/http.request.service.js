"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var Observable_1 = require("rxjs/Observable");
var http_1 = require("@angular/http");
var local_storage_service_1 = require("../../localforage/local-storage.service");
var AppConfig = require("../../../config");
var HttpRequestService = (function (_super) {
    __extends(HttpRequestService, _super);
    function HttpRequestService(backend, defaultOptions, _router) {
        var _this = _super.call(this, backend, defaultOptions) || this;
        _this._router = _router;
        _this.storageService = new local_storage_service_1.LocalStorageService();
        return _this;
    }
    HttpRequestService.prototype.request = function (url, options) {
        return this.intercept(_super.prototype.request.call(this, url, options));
    };
    HttpRequestService.prototype.get = function (url, options) {
        return this.intercept(_super.prototype.get.call(this, this.fullPath(url.toString()), this.getRequestOptionArgs(options)));
    };
    HttpRequestService.prototype.post = function (url, body, options) {
        return this.intercept(_super.prototype.post.call(this, this.fullPath(url.toString()), body, this.getRequestOptionArgs(options)));
    };
    HttpRequestService.prototype.put = function (url, body, options) {
        return this.intercept(_super.prototype.put.call(this, this.fullPath(url.toString()), body, this.getRequestOptionArgs(options)));
    };
    HttpRequestService.prototype.delete = function (url, options) {
        return this.intercept(_super.prototype.delete.call(this, this.fullPath(url.toString()), this.getRequestOptionArgs(options)));
    };
    HttpRequestService.prototype.getDownload = function (url, options) {
        return this.intercept(_super.prototype.get.call(this, this.fullPath(url.toString()), this.getDownloadRequestOptionArgs(options)));
    };
    HttpRequestService.prototype.postDownload = function (url, body, options) {
        return this.intercept(_super.prototype.post.call(this, this.fullPath(url.toString()), body, this.getDownloadRequestOptionArgs(options)));
    };
    HttpRequestService.prototype.postUpload = function (url, file, options) {
        var formData = new FormData();
        formData.append('file', file);
        return this.intercept(_super.prototype.post.call(this, this.fullPath(url.toString()), formData, this.postUploadRequestOptionArgs(options)));
    };
    HttpRequestService.prototype.getRequestOptionArgs = function (options) {
        if (options == null) {
            options = new http_1.RequestOptions();
        }
        if (options.headers == null) {
            options.headers = new http_1.Headers();
            options.headers.append('Authorization', this.storageService.get('access_token'));
            options.headers.append('Content-Type', 'application/json;charset=UTF-8');
        }
        options.withCredentials = true;
        return options;
    };
    HttpRequestService.prototype.getDownloadRequestOptionArgs = function (options) {
        if (options == null) {
            options = new http_1.RequestOptions();
        }
        if (options.headers == null) {
            options.headers = new http_1.Headers();
            options.headers.append('Authorization', this.storageService.get('access_token'));
            options.headers.append('Content-Type', 'application/json;charset=UTF-8');
        }
        options.withCredentials = true;
        options.responseType = http_1.ResponseContentType.Blob;
        return options;
    };
    HttpRequestService.prototype.postUploadRequestOptionArgs = function (options) {
        if (options === null) {
            options = new http_1.RequestOptions();
        }
        else {
            return options;
        }
        if (options.headers === null) {
            options.headers = new http_1.Headers();
            options.headers.append('Authorization', this.storageService.get('access_token'));
            options.headers.append('Content-Type', 'application/json;charset=UTF-8');
        }
        options.withCredentials = true;
        options.responseType = http_1.ResponseContentType.Blob;
        return options;
    };
    HttpRequestService.prototype.getUploadRequestOptionArgs = function (resType, reqContentType) {
        var options = new http_1.RequestOptions();
        if (options.headers == null) {
            options.headers = new http_1.Headers();
            options.headers.append('Authorization', this.storageService.get('access_token'));
            if (null != reqContentType) {
                options.headers.append('Content-Type', reqContentType);
            }
        }
        options.withCredentials = true;
        options.responseType = resType;
        return options;
    };
    HttpRequestService.prototype.fullPath = function (path) {
        return AppConfig.Config.rootService + path;
    };
    HttpRequestService.prototype.intercept = function (observable) {
        var _this = this;
        return observable.catch(function (err, source) {
            if (err.status === 401) {
                var body = JSON.parse(err._body);
                var message = body['message'];
                if (AppConfig.Config.sso) {
                    _this.storageService.save('userInfo', '');
                    window.location.href = AppConfig.Config.rootService + AppConfig.Config.urlLogin;
                }
                else if ('UNAUTHORIZED' === message || undefined === message) {
                    _this.storageService.save('userInfo', '');
                    _this._router.navigate([AppConfig.URL_LOGIN.name]);
                }
                else {
                    window.location.href = message;
                }
                return Observable_1.Observable.empty();
            }
            else {
                return Observable_1.Observable.throw(err);
            }
        });
    };
    HttpRequestService.decorators = [
        { type: core_1.Injectable },
    ];
    /** @nocollapse */
    HttpRequestService.ctorParameters = function () { return [
        { type: http_1.ConnectionBackend, },
        { type: http_1.RequestOptions, },
        { type: router_1.Router, },
    ]; };
    return HttpRequestService;
}(http_1.Http));
exports.HttpRequestService = HttpRequestService;
//# sourceMappingURL=http.request.service.js.map