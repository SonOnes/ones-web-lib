export declare class OnesPaging<T> {
    protected first: boolean;
    protected last: boolean;
    protected number: number;
    protected numberOfElements: number;
    protected size: number;
    totalElements: number;
    private totalPages;
    content: T[];
    constructor(data: any);
}
