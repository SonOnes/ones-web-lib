export declare class OnesMessage {
    code: string;
    message: any;
    type: string;
    violations: any[];
}
