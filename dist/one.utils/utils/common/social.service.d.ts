import { AuthService } from 'angular2-social-login';
export declare class SocialService {
    _auth: AuthService;
    constructor(_auth: AuthService);
    signIn(provider: string): void;
    logout(): void;
}
