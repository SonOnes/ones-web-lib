"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DropdownListItem = (function () {
    function DropdownListItem(id, itemName) {
        this.id = id;
        this.itemName = itemName;
    }
    return DropdownListItem;
}());
exports.DropdownListItem = DropdownListItem;
//# sourceMappingURL=dropdown.list.item.js.map