"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var router_1 = require("@angular/router");
var local_storage_service_1 = require("../../localforage/local-storage.service");
var http_request_service_1 = require("../common/http.request.service");
var OneConfig = require("../../../config");
var OneUserService = (function () {
    function OneUserService(http, router, storageService) {
        this.http = http;
        this.router = router;
        this.storageService = storageService;
    }
    OneUserService.prototype.getLogginUser = function () {
        var user = this.http.get(OneConfig.LOGGING_USER.apiEndpoint).map(function (res) { return res.json(); });
        return user;
    };
    OneUserService.prototype.checkRoles = function (key, user) {
        if (user !== undefined && user !== null && user.roles !== undefined && user.roles.length > 0) {
            for (var obj in user.roles) {
                if (user.roles[obj].toUpperCase() === key.toUpperCase()) {
                    return true;
                }
            }
        }
        return false;
    };
    OneUserService.prototype.checkAuthority = function (key, method, user) {
        if (user !== undefined && user !== null && user.roles !== undefined && user.roles.length > 0) {
            for (var obj in user.apiFunctions) {
                var func = user.apiFunctions[obj];
                if (func.allowAll ||
                    (func.api === key && func.method === method)) {
                    return true;
                }
            }
        }
        return false;
    };
    OneUserService.prototype.login = function (username, pass) {
        var options = new http_1.RequestOptions();
        options.headers = new http_1.Headers();
        options.headers.append('Authorization', OneConfig.Config.loginToken);
        options.headers.append('Content-Type', OneConfig.Config.loginType);
        var token = this.http.post(OneConfig.APP_LOGIN.apiEndpoint, 'grant_type=password&username=' + username + '&password=' + pass + '', options)
            .map(function (res) { return res.json(); });
        return token;
    };
    OneUserService.prototype.logout = function (loginUrl) {
        this.http.get(OneConfig.APP_LOGOUT.apiEndpoint).subscribe(function (data) {
            console.log('logout success');
        }, function (error) { console.log('logout error'); });
        this.storageService.save('userInfo', '');
        this.storageService.save('access_token', '');
        this.storageService.save('refresh_token', '');
        this.router.navigateByUrl('/' + loginUrl);
    };
    OneUserService.decorators = [
        { type: core_1.Injectable },
    ];
    /** @nocollapse */
    OneUserService.ctorParameters = function () { return [
        { type: http_request_service_1.HttpRequestService, },
        { type: router_1.Router, },
        { type: local_storage_service_1.LocalStorageService, },
    ]; };
    return OneUserService;
}());
exports.OneUserService = OneUserService;
//# sourceMappingURL=one.user.service.js.map