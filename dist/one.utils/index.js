"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var http_1 = require("@angular/http");
var forms_1 = require("@angular/forms");
var i18n_module_1 = require("./i18n/i18n.module");
var one_user_service_1 = require("./utils/common/one.user.service");
var filter_1 = require("./filter");
var ng2_toasty_1 = require("ng2-toasty");
var ng_bootstrap_1 = require("@ng-bootstrap/ng-bootstrap");
var OneUtilsModule = (function () {
    function OneUtilsModule() {
    }
    OneUtilsModule.decorators = [
        { type: core_1.NgModule, args: [{
                    imports: [
                        common_1.CommonModule,
                        http_1.HttpModule,
                        i18n_module_1.I18NModule,
                        forms_1.FormsModule,
                        ng2_toasty_1.ToastyModule.forRoot(),
                        ng_bootstrap_1.NgbModule.forRoot()
                    ],
                    declarations: [
                        filter_1.ExponentialStrengthPipe
                    ],
                    exports: [
                        filter_1.ExponentialStrengthPipe
                    ],
                    providers: [
                        one_user_service_1.OneUserService,
                    ],
                },] },
    ];
    /** @nocollapse */
    OneUtilsModule.ctorParameters = function () { return []; };
    return OneUtilsModule;
}());
exports.OneUtilsModule = OneUtilsModule;
//# sourceMappingURL=index.js.map