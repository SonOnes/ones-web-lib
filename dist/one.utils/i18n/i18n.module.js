"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var core_2 = require("@ngx-translate/core");
var http_loader_1 = require("@ngx-translate/http-loader");
var OneConfig = require("../../config");
function createTranslateLoader(http /*, config: Config*/) {
    // let root: string = config.getServiceRoot();
    // if (root.indexOf('localhost') !== -1) {
    // 	return new TranslateStaticLoader(http, 'sumo/mock', '.json');
    // }
    return new http_loader_1.TranslateHttpLoader(http, OneConfig.APP_LANG.apiEndpoint, '');
}
exports.createTranslateLoader = createTranslateLoader;
var I18NModule = (function () {
    function I18NModule(translate /*, private config: Config*/) {
        this.translate = translate; /*, private config: Config*/
        // let root: string = config.getServiceRoot();
        var currentLang = 'language_en.json';
        var langs = ['language_en.json', 'language_vi.json'];
        // if (root.indexOf('localhost') !== -1) {
        // 	currentLang = 'en';
        // 	langs = ['en', 'es'];
        // }
        translate.addLangs(langs);
        translate.setDefaultLang(currentLang);
        translate.use(currentLang);
    }
    I18NModule.forRoot = function () {
        return {
            ngModule: I18NModule
        };
    };
    I18NModule.decorators = [
        { type: core_1.NgModule, args: [{
                    imports: [
                        core_2.TranslateModule.forRoot({
                            loader: {
                                provide: core_2.TranslateLoader,
                                useFactory: createTranslateLoader,
                                deps: [http_1.Http]
                            }
                        })
                    ],
                    declarations: [],
                    exports: [core_2.TranslateModule]
                },] },
    ];
    /** @nocollapse */
    I18NModule.ctorParameters = function () { return [
        { type: core_2.TranslateService, },
    ]; };
    return I18NModule;
}());
exports.I18NModule = I18NModule;
var MessageObj = (function () {
    function MessageObj() {
    }
    return MessageObj;
}());
exports.MessageObj = MessageObj;
//# sourceMappingURL=i18n.module.js.map