import { ModuleWithProviders } from '@angular/core';
import { Http } from '@angular/http';
import { TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
export declare function createTranslateLoader(http: Http): TranslateHttpLoader;
export declare class I18NModule {
    private translate;
    static forRoot(): ModuleWithProviders;
    constructor(translate: TranslateService);
}
export declare class MessageObj {
    key: string;
    value: string;
}
