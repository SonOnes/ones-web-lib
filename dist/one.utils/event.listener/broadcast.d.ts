import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
export interface IBroadcast {
    key: EVENT;
    data?: any;
}
export declare class Broadcast {
    private _eventBus;
    constructor();
    broadcast(key: EVENT, data?: any): void;
    on<T>(key: EVENT): Observable<T>;
}
export declare const enum EVENT {
    SHOW_SPINNER = 0,
    HIDE_SPINNER = 1,
    SHOW_CONFIRM_DIALOG = 2,
    HIDE_CONFIRM_DIALOG = 3,
}
