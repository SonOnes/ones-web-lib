"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Subject_1 = require("rxjs/Subject");
require("rxjs/add/operator/filter");
require("rxjs/add/operator/map");
var Broadcast = (function () {
    function Broadcast() {
        this._eventBus = new Subject_1.Subject();
    }
    Broadcast.prototype.broadcast = function (key, data) {
        this._eventBus.next({ key: key, data: data });
    };
    Broadcast.prototype.on = function (key) {
        return this._eventBus.asObservable()
            .filter(function (event) { return event.key === key; })
            .map(function (event) { return event.data; });
    };
    return Broadcast;
}());
exports.Broadcast = Broadcast;
//# sourceMappingURL=broadcast.js.map