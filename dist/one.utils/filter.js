"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ExponentialStrengthPipe = (function () {
    function ExponentialStrengthPipe() {
    }
    ExponentialStrengthPipe.prototype.transform = function (value, exponent) {
        if (!value) {
            return value;
        }
        for (var i = 0; i < exponent.length; i++) {
            if (exponent[i].id === value) {
                return exponent[i].translate;
            }
        }
        return value;
    };
    ExponentialStrengthPipe.decorators = [
        { type: core_1.Pipe, args: [{ name: 'exponentialStrength' },] },
    ];
    /** @nocollapse */
    ExponentialStrengthPipe.ctorParameters = function () { return []; };
    return ExponentialStrengthPipe;
}());
exports.ExponentialStrengthPipe = ExponentialStrengthPipe;
//# sourceMappingURL=filter.js.map