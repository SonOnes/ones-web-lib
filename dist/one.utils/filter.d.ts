import { PipeTransform } from '@angular/core';
export declare class ExponentialStrengthPipe implements PipeTransform {
    transform(value: any, exponent: any[]): any;
}
