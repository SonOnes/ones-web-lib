"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var LocalStorageService = (function () {
    function LocalStorageService() {
    }
    LocalStorageService.prototype.save = function (name, data) {
        var localData = localStorage.getItem('tinyApp');
        if (localData) {
            localData = JSON.parse(localData);
        }
        else {
            localData = {};
        }
        localData[name] = data;
        localStorage.setItem('tinyApp', JSON.stringify(localData));
    };
    LocalStorageService.prototype.get = function (name) {
        var data = JSON.parse(localStorage.getItem('tinyApp'));
        if (!data) {
            return undefined;
        }
        if (name) {
            if (data[name]) {
                return data[name];
            }
            else {
                return {};
            }
        }
        return data;
    };
    LocalStorageService.decorators = [
        { type: core_1.Injectable },
    ];
    /** @nocollapse */
    LocalStorageService.ctorParameters = function () { return []; };
    return LocalStorageService;
}());
exports.LocalStorageService = LocalStorageService;
//# sourceMappingURL=local-storage.service.js.map