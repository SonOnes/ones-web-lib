export declare class LocalStorageService {
    save(name: string, data: any): void;
    get(name: string): any;
}
