"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
;
;
exports.Config = {
    sso: true,
    rootService: '',
    urlLogin: '',
    loginToken: '',
    loginType: 'application/x-www-form-urlencoded',
    MAX_RESULT: 100,
    defaultCountry: 1,
    dateFormat: 'dd-MM-yyyy HH:mm:ss',
    dateGFormat: 'dd-MM-yyyy',
    dateRFormat: 'dd-mm-yyyy',
    singleDropdownSettings: {
        singleSelection: true,
        text: 'Select',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        classes: 'custom-class'
    },
    multiDropdownSettings: {
        singleSelection: false,
        text: 'Select',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        enableSearchFilter: true,
        classes: 'custom-class'
    },
    TOAST_MESSAGE_TIMEOUT: 4,
    toastTheme: 'bootstrap',
    importTemplateType: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
};
exports.APP_LANG = {
    apiEndpoint: 'app/config/',
    method: 'GET'
};
/* common page url */
exports.ROOT_URL = {
    name: ''
};
exports.URL_LOGIN = {
    name: 'login'
};
exports.URL_ABOUT = {
    name: 'about'
};
exports.URL_SUCCESS = {
    name: 'success'
};
exports.URL_SUCCESS_SSO = {
    name: 'success/:type/:token'
};
exports.URL_PROFILE = {
    name: 'profile'
};
/* org url*/
exports.URL_MY_ORGS = {
    name: 'myorgs'
};
exports.URL_ORG_MGNT = {
    name: 'myorg/detail/:id'
};
exports.URL_EDIT_ORG = {
    name: 'myorg/:id'
};
exports.URL_CREATE_ORG = {
    name: 'myorg/create'
};
/* manage function and authorization */
exports.URL_FUNCTIONS = {
    name: 'functions'
};
exports.URL_EDIT_FUNCTION = {
    name: 'function/edit/:id'
};
exports.URL_FUNCTION = {
    name: 'function/detail/:id'
};
exports.URL_CREATE_FUNCTION = {
    name: 'function/create'
};
// authentication apis
exports.LOGGING_USER = {
    apiEndpoint: '/api/usersso/userinf',
    method: 'GET'
};
exports.APP_LOGIN = {
    apiEndpoint: '/api/nonauth/login',
    method: 'GET'
};
exports.APP_LOGOUT = {
    apiEndpoint: '/logout',
    method: 'GET'
};
/* authorization api */
exports.FUNCTION_GET_LIST = {
    apiEndpoint: '/api/functions',
    method: 'POST'
};
exports.FUNCTION_GET_DETAIL = {
    apiEndpoint: '/api/function',
    method: 'GET'
};
exports.FUNCTION_EDIT_OBJ = {
    apiEndpoint: '/api/function',
    method: 'POST'
};
exports.FUNCTION_EXPORT = {
    apiEndpoint: '/api/functions/export',
    method: 'POST'
};
/* ref data api */
exports.COUNTRIES_API = {
    apiEndpoint: '/api/countrys',
    method: 'POST'
};
exports.CITIES_API = {
    apiEndpoint: '/api/citys',
    method: 'POST'
};
exports.DISTRICT_API = {
    apiEndpoint: '/api/districts',
    method: 'GET'
};
/* management api */
// org
exports.MY_ORG_GET_LIST = {
    apiEndpoint: '/api/myorgs',
    method: 'POST'
};
exports.MY_ORGS_GET_DETAIL = {
    apiEndpoint: '/api/mgnt/organization',
    method: 'POST'
};
exports.MY_ORGS_EDIT_OBJ = {
    apiEndpoint: '/api/mgnt/organization',
    method: 'POST'
};
exports.MY_ORGS_DEL_OBJ = {
    apiEndpoint: '/api/mgnt/organization',
    method: 'POST'
};
exports.MY_ORGS_EXPORT = {
    apiEndpoint: '/api/mgnt/organizations/export',
    method: 'POST'
};
// authority
exports.AUTHORITY_GET_LIST = {
    apiEndpoint: '/api/mgnt/authoritys',
    method: 'POST'
};
exports.AUTHORITY_GET_DETAIL = {
    apiEndpoint: '/api/mgnt/authority',
    method: 'GET'
};
exports.AUTHORITY_DEL_OBJ = {
    apiEndpoint: '/api/mgnt/authority',
    method: 'DETETE'
};
exports.AUTHORITY_DEL_OBJs = {
    apiEndpoint: '/api/mgnt/authoritys',
    method: 'DELETE'
};
exports.AUTHORITY_EDIT_OBJ = {
    apiEndpoint: '/api/mgnt/authority',
    method: 'POST'
};
// user
exports.USER_GET_LIST = {
    apiEndpoint: '/api/mgnt/users',
    method: 'POST'
};
exports.USER_GET_DETAIL = {
    apiEndpoint: '/api/mgnt/user',
    method: 'GET'
};
exports.USER_DEL_OBJ = {
    apiEndpoint: '/api/mgnt/user',
    method: 'DETETE'
};
exports.USER_EDIT_OBJ = {
    apiEndpoint: '/api/mgnt/user',
    method: 'POST'
};
exports.USERAUTHORITIES_GET_LIST = {
    apiEndpoint: '/api/mgnt/userAuthoritiess',
    method: 'POST'
};
exports.USERAUTHORITIES_GET_DETAIL = {
    apiEndpoint: '/api/mgnt/userAuthorities',
    method: 'GET'
};
exports.USERAUTHORITIES_DEL_OBJ = {
    apiEndpoint: '/api/mgnt/userAuthorities',
    method: 'DETETE'
};
exports.USERAUTHORITIES_EDIT_OBJ = {
    apiEndpoint: '/api/mgnt/userAuthorities',
    method: 'POST'
};
exports.USERINFORMATION_GET_LIST = {
    apiEndpoint: '/api/mgnt/userInformations',
    method: 'POST'
};
exports.USERINFORMATION_GET_DETAIL = {
    apiEndpoint: '/api/mgnt/userInformation',
    method: 'GET'
};
exports.USERINFORMATION_DEL_OBJ = {
    apiEndpoint: '/api/mgnt/userInformation',
    method: 'DETETE'
};
exports.USERINFORMATION_DEL_OBJs = {
    apiEndpoint: '/api/mgnt/userInformations',
    method: 'DELETE'
};
exports.USERINFORMATION_EDIT_OBJ = {
    apiEndpoint: '/api/mgnt/userInformation',
    method: 'POST'
};
//# sourceMappingURL=config.js.map