"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var utils_1 = require("../one.utils/utils/common/utils");
var CustomValidators = (function () {
    function CustomValidators() {
    }
    CustomValidators.emailValidator = function (control) {
        var regex = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if (control.value.match(regex)) {
            return null;
        }
        else {
            return { 'validEmail': true };
        }
    };
    CustomValidators.numberValidator = function (control) {
        if (control.value >= 0 && control.value < 1000000000) {
            return null;
        }
        else {
            return { 'inValidNumber': true };
        }
    };
    CustomValidators.noLongerThan1000Validator = function (control) {
        if (control.value >= 0 && control.value <= 1000) {
            return null;
        }
        else {
            return { 'inValidNumber1000': true };
        }
    };
    CustomValidators.noSpaceValidator = function (control) {
        return (control.value && control.value.trim() == "") ? { "isSpace": true } : null;
    };
    CustomValidators.startDateEndDate = function (start, end) {
        var utils = new utils_1.Utils();
        return function (group) {
            var startDate = group.controls[start];
            var endDate = group.controls[end];
            if (startDate.value && endDate.value) {
                if ((new Date(utils.parseJVDate(startDate.value))) > (new Date(utils.parseJVDate(endDate.value)))) {
                    return {
                        validationError: true
                    };
                }
                else {
                    return null;
                }
            }
        };
    };
    return CustomValidators;
}());
exports.CustomValidators = CustomValidators;
//# sourceMappingURL=CustomValidators.js.map