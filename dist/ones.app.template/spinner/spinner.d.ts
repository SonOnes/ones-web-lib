import { OnInit } from '@angular/core';
import { Broadcast } from '../../one.utils/event.listener/broadcast';
export declare class SpinnerComponent implements OnInit {
    private broadcast;
    isShow: boolean;
    constructor(broadcast: Broadcast);
    ngOnInit(): void;
}
