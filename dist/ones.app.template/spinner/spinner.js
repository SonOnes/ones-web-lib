"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var broadcast_1 = require("../../one.utils/event.listener/broadcast");
var SpinnerComponent = (function () {
    function SpinnerComponent(broadcast) {
        this.broadcast = broadcast;
    }
    SpinnerComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.broadcast.on(0 /* SHOW_SPINNER */)
            .subscribe(function (message) {
            _this.isShow = true;
        });
        this.broadcast.on(1 /* HIDE_SPINNER */)
            .subscribe(function (message) {
            _this.isShow = false;
        });
    };
    SpinnerComponent.decorators = [
        { type: core_1.Component, args: [{
                    selector: 'page-spinner',
                    //	template: require('./spinner.html')
                    templateUrl: './static/spinner.html'
                },] },
    ];
    /** @nocollapse */
    SpinnerComponent.ctorParameters = function () { return [
        { type: broadcast_1.Broadcast, },
    ]; };
    return SpinnerComponent;
}());
exports.SpinnerComponent = SpinnerComponent;
//# sourceMappingURL=spinner.js.map