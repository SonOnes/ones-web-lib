"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var http_1 = require("@angular/http");
var i18n_module_1 = require("../one.utils/i18n/i18n.module");
var forms_1 = require("@angular/forms");
var header_1 = require("./header");
var title_1 = require("./title");
var footer_1 = require("./footer");
var success_component_1 = require("./success.component");
var login_1 = require("./login");
var about_1 = require("./about");
var spinner_1 = require("./spinner/spinner");
var basic_1 = require("./dialogs/basic");
var click_out_site_1 = require("../ones.app.template/directives/click.out.site");
var autofocus_directives_1 = require("../ones.app.template/directives/autofocus.directives");
var one_user_service_1 = require("../one.utils/utils/common/one.user.service");
var utils_1 = require("../one.utils/utils/common/utils");
var ng2_toasty_1 = require("ng2-toasty");
var ng_bootstrap_1 = require("@ng-bootstrap/ng-bootstrap");
var TemplateOneAppModule = (function () {
    function TemplateOneAppModule() {
    }
    TemplateOneAppModule.decorators = [
        { type: core_1.NgModule, args: [{
                    imports: [
                        common_1.CommonModule,
                        http_1.HttpModule,
                        i18n_module_1.I18NModule,
                        forms_1.FormsModule,
                        ng2_toasty_1.ToastyModule.forRoot(),
                        ng_bootstrap_1.NgbModule.forRoot()
                    ],
                    declarations: [
                        autofocus_directives_1.AutofocusDirectives,
                        click_out_site_1.ClickOutSiteDirective,
                        header_1.HeaderComponent,
                        title_1.TitleComponent,
                        footer_1.FooterComponent,
                        success_component_1.SuccessComponent,
                        login_1.LoginComponent,
                        about_1.AboutComponent,
                        spinner_1.SpinnerComponent,
                        basic_1.BasicDialogComponent
                    ],
                    exports: [
                        autofocus_directives_1.AutofocusDirectives,
                        click_out_site_1.ClickOutSiteDirective,
                        header_1.HeaderComponent,
                        title_1.TitleComponent,
                        footer_1.FooterComponent,
                        success_component_1.SuccessComponent,
                        login_1.LoginComponent,
                        about_1.AboutComponent,
                        spinner_1.SpinnerComponent,
                        basic_1.BasicDialogComponent
                    ],
                    providers: [
                        one_user_service_1.OneUserService,
                        utils_1.Utils
                    ],
                },] },
    ];
    /** @nocollapse */
    TemplateOneAppModule.ctorParameters = function () { return []; };
    return TemplateOneAppModule;
}());
exports.TemplateOneAppModule = TemplateOneAppModule;
//# sourceMappingURL=index.js.map