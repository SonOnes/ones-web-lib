import { LocalStorageService } from '../../one.utils/localforage/local-storage.service';
import { OneUserService } from '../../one.utils/utils/common/one.user.service';
import { Utils } from '../../one.utils/utils/common/utils';
import { RefdataService } from '../../one.utils/refdata/refdata.service';
import { Base } from './base';
export declare abstract class BaseEditComponent extends Base {
    protected storageService: LocalStorageService;
    protected utils: Utils;
    protected oneUserService: OneUserService;
    protected refdataService: RefdataService;
    constructor(storageService: LocalStorageService, utils: Utils, oneUserService: OneUserService, refdataService: RefdataService);
}
