import { ElementRef } from '@angular/core';
import { GridOptions } from 'ag-grid/main';
import { LocalStorageService } from '../../one.utils/localforage/local-storage.service';
import { OneUserService } from '../../one.utils/utils/common/one.user.service';
import { Utils } from '../../one.utils/utils/common/utils';
import { RefdataService } from '../../one.utils/refdata/refdata.service';
import { Base } from './base';
export declare abstract class BaseComponent extends Base {
    protected storageService: LocalStorageService;
    protected utils: Utils;
    protected oneUserService: OneUserService;
    protected refdataService: RefdataService;
    protected gridOptions: GridOptions;
    protected showGrid: boolean;
    protected rowData: any[];
    protected columnDefs: any[];
    protected gridApi: any;
    protected gridColumnApi: any;
    protected components: any;
    protected rowCount: string;
    protected reload: boolean;
    protected fileInput: ElementRef;
    constructor(storageService: LocalStorageService, utils: Utils, oneUserService: OneUserService, refdataService: RefdataService);
    protected import(): void;
}
