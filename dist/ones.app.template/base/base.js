"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var moment = require("moment");
var config_1 = require("../../config");
var one_obj_1 = require("../../one.utils/utils/common/one.obj");
var city_filter_1 = require("../../one.utils/refdata/city.filter");
var country_filter_1 = require("../../one.utils/refdata/country.filter");
var Base = (function () {
    function Base(storageService, utils, oneUserService, refdataService) {
        this.storageService = storageService;
        this.utils = utils;
        this.oneUserService = oneUserService;
        this.refdataService = refdataService;
        this.countryIdDropdown = new one_obj_1.Dropdown();
        this.provinceIdDropdown = new one_obj_1.Dropdown();
        this.alerts = [];
        this.countries = [];
        this.boolDefs = [];
        this.myDateRangeOptions = {
            dateFormat: config_1.Config.dateRFormat,
        };
        this.appUser = this.storageService.get('userInfo');
        this.dateFormat = config_1.Config.dateFormat;
        this.countryIdDropdown = new one_obj_1.Dropdown();
        this.provinceIdDropdown = new one_obj_1.Dropdown();
        this.countrySelected = config_1.Config.defaultCountry;
        this.countryIdDropdown.settings = config_1.Config.singleDropdownSettings;
        this.provinceIdDropdown.settings = config_1.Config.multiDropdownSettings;
    }
    Base.prototype.ngOnInit = function () {
        // tbd
    };
    Base.prototype.ngAfterViewInit = function () {
        // tbd
    };
    Base.prototype.toUTCDate = function (date) {
        var _utc = new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate());
        return _utc;
    };
    ;
    Base.prototype.millisToUTCDate = function (millis) {
        if (millis) {
            return this.toUTCDate(new Date(millis));
        }
    };
    ;
    Base.prototype.checkRoles = function (key) {
        return this.oneUserService.checkRoles(key, this.appUser);
    };
    Base.prototype.checkAuthority = function (api, method) {
        return this.oneUserService.checkAuthority(api, method, this.appUser);
    };
    Base.prototype.closeAlert = function (i) {
        this.alerts.splice(i, 1);
    };
    Base.prototype.checkStrEmplty = function (str) {
        if (null == str || '' === str) {
            return true;
        }
        return false;
    };
    Base.prototype.getCountries = function () {
        var _this = this;
        var cfilter = new country_filter_1.CountryFilter();
        cfilter.limit = 500;
        if (null == Base.scountries || 0 === Base.scountries.length) {
            this.refdataService.searchCounties(cfilter).subscribe(function (data) {
                var results = new one_obj_1.OnesPaging(data);
                Base.scountries = results.content;
                _this.countries = Base.scountries;
                _this.countryIdDropdown.listItem = new one_obj_1.DropdownList(null, _this.countries).items;
                _this.countryIdDropdown.itemsSelected = _this.utils.selectValue(_this.countrySelected, _this.countryIdDropdown.listItem);
            }, function (error) {
                _this.alerts = _this.utils.handleError(error, _this.alerts);
            });
        }
        else {
            this.countries = Base.scountries;
            this.countryIdDropdown.listItem = new one_obj_1.DropdownList(null, this.countries).items;
            this.countryIdDropdown.itemsSelected = this.utils.selectValue(this.countrySelected, this.countryIdDropdown.listItem);
        }
    };
    Base.prototype.onSelectCountryId = function ($event) {
        var _this = this;
        if (null == this.countryIdDropdown.itemsSelected || 0 === this.countryIdDropdown.itemsSelected.length) {
            this.provinceIdDropdown.listItem = [];
            this.provinceIdDropdown.itemsSelected = [];
            return;
        }
        var cfilter = new city_filter_1.CityFilter();
        cfilter.limit = 500;
        cfilter.countryId = this.utils.parseSelectValues(this.countryIdDropdown.itemsSelected)[0];
        this.refdataService.searchCities(cfilter).subscribe(function (data) {
            var results = new one_obj_1.OnesPaging(data);
            _this.provinceIdDropdown.listItem = new one_obj_1.DropdownList(null, results.content).items;
        }, function (error) {
            _this.alerts = _this.utils.handleError(error, _this.alerts);
        });
    };
    Base.prototype.getCity = function () {
        var _this = this;
        var cfilter = new city_filter_1.CityFilter();
        cfilter.limit = 500;
        cfilter.countryId = this.countrySelected;
        this.refdataService.searchCities(cfilter).subscribe(function (data) {
            var results = new one_obj_1.OnesPaging(data);
            _this.provinceIdDropdown.listItem = new one_obj_1.DropdownList(null, results.content).items;
        }, function (error) {
            _this.alerts = _this.utils.handleError(error, _this.alerts);
        });
    };
    Base.prototype.OnDeSelectCountryId = function ($event) {
        this.provinceIdDropdown.itemsSelected = [];
    };
    Base.prototype.formatGridDate = function (data) {
        var result = data.value;
        if ('' === result || null === result || undefined === result) {
            return '';
        }
        return moment(new Date(result)).format('' + config_1.Config.dateGFormat);
    };
    ;
    Base.prototype.checkIconType = function (icon) {
        if (null === icon || undefined === icon) {
            return 0;
        }
        if (icon.indexOf('fa-') === 0) {
            return 1;
        }
        if (icon.indexOf('glyphicon') === 0) {
            return 2;
        }
        return 0;
    };
    Base.prototype.onFocusOut = function ($event) {
        var target = $event.target || $event.srcElement || $event.currentTarget;
        target.click();
    };
    Base.scountries = [];
    return Base;
}());
exports.Base = Base;
//# sourceMappingURL=base.js.map