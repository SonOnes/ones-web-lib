"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var one_obj_1 = require("../../one.utils/utils/common/one.obj");
var base_1 = require("./base");
var BaseComponent = (function (_super) {
    __extends(BaseComponent, _super);
    function BaseComponent(storageService, utils, oneUserService, refdataService) {
        var _this = _super.call(this, storageService, utils, oneUserService, refdataService) || this;
        _this.storageService = storageService;
        _this.utils = utils;
        _this.oneUserService = oneUserService;
        _this.refdataService = refdataService;
        _this.reload = false;
        _this.reload = false;
        _this.rowData = [];
        _this.columnDefs = [];
        _this.boolDefs = [];
        _this.boolDefs.push(new one_obj_1.DropdownListItem(null, 'Both'));
        _this.boolDefs.push(new one_obj_1.DropdownListItem(true, 'True'));
        _this.boolDefs.push(new one_obj_1.DropdownListItem(false, 'False'));
        _this.components = {
            loadingRenderer: function (params) {
                if (params.value !== undefined) {
                    return params.value;
                }
                else {
                    return '<img src="../../../images/spinner.gif">';
                }
            }
        };
        return _this;
    }
    BaseComponent.prototype.import = function () {
        var event = new MouseEvent('click', { bubbles: false });
        this.fileInput.nativeElement.dispatchEvent(event);
    };
    BaseComponent.propDecorators = {
        "fileInput": [{ type: core_1.ViewChild, args: ['fileInput',] },],
    };
    return BaseComponent;
}(base_1.Base));
exports.BaseComponent = BaseComponent;
//# sourceMappingURL=base.component.js.map