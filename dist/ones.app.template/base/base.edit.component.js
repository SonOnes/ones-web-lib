"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var config_1 = require("../../config");
var base_1 = require("./base");
var BaseEditComponent = (function (_super) {
    __extends(BaseEditComponent, _super);
    function BaseEditComponent(storageService, utils, oneUserService, refdataService) {
        var _this = _super.call(this, storageService, utils, oneUserService, refdataService) || this;
        _this.storageService = storageService;
        _this.utils = utils;
        _this.oneUserService = oneUserService;
        _this.refdataService = refdataService;
        _this.provinceIdDropdown.settings = config_1.Config.singleDropdownSettings;
        return _this;
    }
    return BaseEditComponent;
}(base_1.Base));
exports.BaseEditComponent = BaseEditComponent;
//# sourceMappingURL=base.edit.component.js.map