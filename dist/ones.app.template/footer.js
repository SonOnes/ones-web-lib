"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var FooterComponent = (function () {
    function FooterComponent() {
    }
    FooterComponent.decorators = [
        { type: core_1.Component, args: [{
                    selector: 'page-footer',
                    //  template: require('./footer.html')
                    templateUrl: './static/footer.html'
                },] },
    ];
    /** @nocollapse */
    FooterComponent.ctorParameters = function () { return []; };
    return FooterComponent;
}());
exports.FooterComponent = FooterComponent;
//# sourceMappingURL=footer.js.map