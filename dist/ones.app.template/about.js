"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var AboutComponent = (function () {
    function AboutComponent() {
    }
    AboutComponent.decorators = [
        { type: core_1.Component, args: [{
                    selector: 'page-about',
                    //  template: require('./about.html')
                    templateUrl: './static/about.html'
                },] },
    ];
    /** @nocollapse */
    AboutComponent.ctorParameters = function () { return []; };
    return AboutComponent;
}());
exports.AboutComponent = AboutComponent;
//# sourceMappingURL=about.js.map