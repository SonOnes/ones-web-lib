"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ClickOutSiteDirective = (function () {
    function ClickOutSiteDirective(el) {
        this.el = el;
        this.tohClickOutSite = new core_1.EventEmitter();
    }
    ClickOutSiteDirective.prototype.documentClick = function ($event) {
        if (!this.el.nativeElement.contains($event.target)) {
            this.tohClickOutSite.emit(null);
        }
    };
    ClickOutSiteDirective.decorators = [
        { type: core_1.Directive, args: [{
                    selector: '[tohClickOutSite]'
                },] },
    ];
    /** @nocollapse */
    ClickOutSiteDirective.ctorParameters = function () { return [
        { type: core_1.ElementRef, },
    ]; };
    ClickOutSiteDirective.propDecorators = {
        "tohClickOutSite": [{ type: core_1.Output },],
        "documentClick": [{ type: core_1.HostListener, args: ['document:click', ['$event'],] },],
    };
    return ClickOutSiteDirective;
}());
exports.ClickOutSiteDirective = ClickOutSiteDirective;
//# sourceMappingURL=click.out.site.js.map