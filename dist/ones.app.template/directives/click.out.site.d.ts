import { ElementRef, EventEmitter } from '@angular/core';
export declare class ClickOutSiteDirective {
    private el;
    tohClickOutSite: EventEmitter<{}>;
    constructor(el: ElementRef);
    documentClick($event: any): void;
}
