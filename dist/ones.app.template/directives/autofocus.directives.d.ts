import { AfterViewInit, ElementRef } from '@angular/core';
export declare class AutofocusDirectives implements AfterViewInit {
    private el;
    constructor(el: ElementRef);
    ngAfterViewInit(): void;
}
