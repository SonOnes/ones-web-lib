"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var AutofocusDirectives = (function () {
    function AutofocusDirectives(el) {
        this.el = el;
    }
    AutofocusDirectives.prototype.ngAfterViewInit = function () {
        this.el.nativeElement.focus();
    };
    AutofocusDirectives.decorators = [
        { type: core_1.Directive, args: [{
                    selector: '[appAutofocus]'
                },] },
    ];
    /** @nocollapse */
    AutofocusDirectives.ctorParameters = function () { return [
        { type: core_1.ElementRef, },
    ]; };
    return AutofocusDirectives;
}());
exports.AutofocusDirectives = AutofocusDirectives;
//# sourceMappingURL=autofocus.directives.js.map