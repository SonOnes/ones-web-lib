"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var local_storage_service_1 = require("../one.utils/localforage/local-storage.service");
var config_1 = require("../config");
var broadcast_1 = require("../one.utils/event.listener/broadcast");
var one_user_service_1 = require("../one.utils/utils/common/one.user.service");
var utils_1 = require("../one.utils/utils/common/utils");
var SuccessComponent = (function () {
    function SuccessComponent(storageService, route, utils, broadcast, userService, router) {
        this.storageService = storageService;
        this.route = route;
        this.utils = utils;
        this.broadcast = broadcast;
        this.userService = userService;
        this.router = router;
        this.alerts = [];
    }
    SuccessComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.broadcast.broadcast(0 /* SHOW_SPINNER */);
        var tokenType = '';
        var token = '';
        this.route.params.forEach(function (params) {
            tokenType = params['type'];
            token = params['token'];
        });
        if (null != tokenType && undefined !== tokenType) {
            this.storageService.save('access_token', tokenType + ' ' + token);
        }
        this.userService.getLogginUser().subscribe(function (data) {
            _this.storageService.save('userInfo', data);
            _this.gotoHome();
            _this.broadcast.broadcast(1 /* HIDE_SPINNER */);
        }, function (error) {
            _this.alerts = _this.utils.handleError(error, _this.alerts);
            _this.broadcast.broadcast(1 /* HIDE_SPINNER */);
        });
    };
    SuccessComponent.prototype.gotoHome = function () {
        var auth = this.storageService.get('url_before_auth');
        if (auth === undefined || auth === null || auth === '' || JSON.stringify(auth) === JSON.stringify({})) {
            var address = [config_1.ROOT_URL.name];
            this.router.navigate(address);
        }
        else {
            var link = [auth];
            this.router.navigate(link);
        }
        this.storageService.save('url_before_auth', '');
    };
    SuccessComponent.decorators = [
        { type: core_1.Component, args: [{
                    template: ''
                },] },
    ];
    /** @nocollapse */
    SuccessComponent.ctorParameters = function () { return [
        { type: local_storage_service_1.LocalStorageService, },
        { type: router_1.ActivatedRoute, },
        { type: utils_1.Utils, },
        { type: broadcast_1.Broadcast, },
        { type: one_user_service_1.OneUserService, },
        { type: router_1.Router, },
    ]; };
    return SuccessComponent;
}());
exports.SuccessComponent = SuccessComponent;
//# sourceMappingURL=success.component.js.map