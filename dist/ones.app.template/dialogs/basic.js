"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var broadcast_1 = require("../../one.utils/event.listener/broadcast");
var BasicDialogComponent = (function () {
    function BasicDialogComponent(broadcast) {
        this.broadcast = broadcast;
    }
    BasicDialogComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.broadcast.on(2 /* SHOW_CONFIRM_DIALOG */)
            .subscribe(function (data) {
            _this.dialogObj = data;
            _this.isShowDialogConfirm = true;
        });
        this.broadcast.on(3 /* HIDE_CONFIRM_DIALOG */)
            .subscribe(function (data) {
            _this.isShowDialogConfirm = false;
        });
    };
    BasicDialogComponent.prototype.cancel = function () {
        this.broadcast.broadcast(3 /* HIDE_CONFIRM_DIALOG */);
    };
    BasicDialogComponent.prototype.ok = function () {
        this.cancel();
        this.dialogObj.ok();
    };
    BasicDialogComponent.decorators = [
        { type: core_1.Component, args: [{
                    selector: 'basic-dialog',
                    //	template: require('./basic.html')
                    templateUrl: './static/basic.html'
                },] },
    ];
    /** @nocollapse */
    BasicDialogComponent.ctorParameters = function () { return [
        { type: broadcast_1.Broadcast, },
    ]; };
    return BasicDialogComponent;
}());
exports.BasicDialogComponent = BasicDialogComponent;
var DialogObj = (function () {
    function DialogObj(ok) {
        this.okButtonLabel = 'Yes';
        this.cancelButtonLabel = 'No';
        this.title = 'Warning';
        this.message = 'Are you sure?';
        this.clickOutSiteToClose = true;
        this.ok = ok;
    }
    return DialogObj;
}());
exports.DialogObj = DialogObj;
//# sourceMappingURL=basic.js.map