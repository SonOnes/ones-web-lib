import { OnInit } from '@angular/core';
import { Broadcast } from '../../one.utils/event.listener/broadcast';
export declare class BasicDialogComponent implements OnInit {
    private broadcast;
    isShowDialogConfirm: boolean;
    protected dialogObj: DialogObj;
    constructor(broadcast: Broadcast);
    ngOnInit(): void;
    cancel(): void;
    ok(): void;
}
export declare class DialogObj {
    message: string;
    title: string;
    okButtonLabel: string;
    cancelButtonLabel: string;
    clickOutSiteToClose: boolean;
    ok: (data?: any) => void;
    constructor(ok: (data?: any) => void);
}
