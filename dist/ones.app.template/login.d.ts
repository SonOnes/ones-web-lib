import { Router } from '@angular/router';
import { WindowRefService } from '../one.utils/utils/common/window-ref.service';
import { Broadcast } from '../one.utils/event.listener/broadcast';
import { OneUserService } from '../one.utils/utils/common/one.user.service';
import { BaseComponent } from './base/base.component';
import { LocalStorageService } from '../one.utils/localforage/local-storage.service';
import { Utils } from '../one.utils/utils/common/utils';
import { RefdataService } from '../one.utils/refdata/refdata.service';
import '../../style/login.scss';
export declare class LoginComponent extends BaseComponent {
    userService: OneUserService;
    storageService: LocalStorageService;
    protected utils: Utils;
    private broadcast;
    private router;
    protected refdataService: RefdataService;
    private _window;
    private username;
    private pass;
    private usernameValid;
    private passValid;
    formNum: number;
    constructor(userService: OneUserService, storageService: LocalStorageService, utils: Utils, broadcast: Broadcast, router: Router, windowRef: WindowRefService, refdataService: RefdataService);
    ngOnInit(): void;
    login(): void;
}
