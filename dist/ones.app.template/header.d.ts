import { Router } from '@angular/router';
import { Utils } from '../one.utils/utils/common/utils';
import { LocalStorageService } from '../one.utils/localforage/local-storage.service';
import { OneUserService } from '../one.utils/utils/common/one.user.service';
import { BaseComponent } from '../ones.app.template/base/base.component';
import { RefdataService } from '../one.utils/refdata/refdata.service';
export declare class HeaderComponent extends BaseComponent {
    protected userService: OneUserService;
    protected utils: Utils;
    private router;
    storageService: LocalStorageService;
    protected refdataService: RefdataService;
    hmenu1: boolean;
    hmenu2: boolean;
    hmenu3: boolean;
    hmenu4: boolean;
    strUser: string;
    constructor(userService: OneUserService, utils: Utils, router: Router, storageService: LocalStorageService, refdataService: RefdataService);
    ngOnInit(): void;
    gotoHome(): void;
    gotoMyProfile(): void;
    gotoManagement(): void;
    logout(): void;
}
