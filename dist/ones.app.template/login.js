"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var http_1 = require("@angular/http");
var window_ref_service_1 = require("../one.utils/utils/common/window-ref.service");
var one_obj_1 = require("../one.utils/utils/common/one.obj");
var broadcast_1 = require("../one.utils/event.listener/broadcast");
var one_user_service_1 = require("../one.utils/utils/common/one.user.service");
var base_component_1 = require("./base/base.component");
var local_storage_service_1 = require("../one.utils/localforage/local-storage.service");
var utils_1 = require("../one.utils/utils/common/utils");
var config_1 = require("../config");
var refdata_service_1 = require("../one.utils/refdata/refdata.service");
require("../../style/login.scss");
var LoginComponent = (function (_super) {
    __extends(LoginComponent, _super);
    function LoginComponent(userService, storageService, utils, broadcast, router, windowRef, refdataService) {
        var _this = _super.call(this, storageService, utils, userService, refdataService) || this;
        _this.userService = userService;
        _this.storageService = storageService;
        _this.utils = utils;
        _this.broadcast = broadcast;
        _this.router = router;
        _this.refdataService = refdataService;
        _this.username = null;
        _this.pass = null;
        _this.usernameValid = false;
        _this.passValid = false;
        _this._window = windowRef.nativeWindow;
        _this.formNum = 1;
        return _this;
    }
    LoginComponent.prototype.ngOnInit = function () {
        // tbd
    };
    LoginComponent.prototype.login = function () {
        var _this = this;
        this.broadcast.broadcast(0 /* SHOW_SPINNER */);
        var token;
        this.alerts = [];
        this.userService.login(this.username, this.pass).subscribe(function (data) {
            token = data;
            _this.storageService.save('access_token', token.token_type + ' ' + token.access_token);
            _this.storageService.save('refresh_token', token.token_type + ' ' + token.refresh_token);
            _this.userService.getLogginUser().subscribe(function (data) {
                _this.appUser = data;
                _this.storageService.save('userInfo', _this.appUser);
                _this.router.navigate([config_1.URL_SUCCESS.name]);
            }, function (error) {
                _this.alerts = _this.utils.handleError(error, _this.alerts);
            });
            _this.broadcast.broadcast(1 /* HIDE_SPINNER */);
        }, function (error) {
            _this.broadcast.broadcast(1 /* HIDE_SPINNER */);
            var message = new one_obj_1.OnesMessage();
            message.type = 'danger';
            if (error instanceof http_1.Response) {
                var body = error.json() || '';
                var err = body.error || JSON.stringify(body);
                console.log('err: ' + err);
                message.code = body.code;
                message.message = body.message;
            }
            _this.alerts.push({ msg: message.message, type: message.type, closable: true });
        });
    };
    LoginComponent.decorators = [
        { type: core_1.Component, args: [{
                    selector: 'page-login',
                    //  template: require('./login.html')
                    templateUrl: './static/login.html'
                },] },
    ];
    /** @nocollapse */
    LoginComponent.ctorParameters = function () { return [
        { type: one_user_service_1.OneUserService, },
        { type: local_storage_service_1.LocalStorageService, },
        { type: utils_1.Utils, },
        { type: broadcast_1.Broadcast, },
        { type: router_1.Router, },
        { type: window_ref_service_1.WindowRefService, },
        { type: refdata_service_1.RefdataService, },
    ]; };
    return LoginComponent;
}(base_component_1.BaseComponent));
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=login.js.map