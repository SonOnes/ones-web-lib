import { OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalStorageService } from '../one.utils/localforage/local-storage.service';
import { Broadcast } from '../one.utils/event.listener/broadcast';
import { OneUserService } from '../one.utils/utils/common/one.user.service';
import { Utils } from '../one.utils/utils/common/utils';
export declare class SuccessComponent implements OnInit {
    private storageService;
    private route;
    private utils;
    private broadcast;
    userService: OneUserService;
    private router;
    private alerts;
    constructor(storageService: LocalStorageService, route: ActivatedRoute, utils: Utils, broadcast: Broadcast, userService: OneUserService, router: Router);
    ngOnInit(): void;
    gotoHome(): void;
}
