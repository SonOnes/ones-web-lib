import { FormControl, FormGroup } from '@angular/forms';
export declare class CustomValidators {
    static emailValidator(control: FormControl): {
        'validEmail': boolean;
    };
    static numberValidator(control: FormControl): {
        'inValidNumber': boolean;
    };
    static noLongerThan1000Validator(control: FormControl): {
        'inValidNumber1000': boolean;
    };
    static noSpaceValidator(control: FormControl): {
        "isSpace": boolean;
    };
    static startDateEndDate(start: any, end: any): (group: FormGroup) => {
        [key: string]: any;
    };
}
