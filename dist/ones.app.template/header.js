"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var Config = require("../config");
var utils_1 = require("../one.utils/utils/common/utils");
var local_storage_service_1 = require("../one.utils/localforage/local-storage.service");
var one_user_service_1 = require("../one.utils/utils/common/one.user.service");
var base_component_1 = require("../ones.app.template/base/base.component");
var refdata_service_1 = require("../one.utils/refdata/refdata.service");
var HeaderComponent = (function (_super) {
    __extends(HeaderComponent, _super);
    function HeaderComponent(userService, utils, router, storageService, refdataService) {
        var _this = _super.call(this, storageService, utils, userService, refdataService) || this;
        _this.userService = userService;
        _this.utils = utils;
        _this.router = router;
        _this.storageService = storageService;
        _this.refdataService = refdataService;
        _this.hmenu1 = false;
        _this.hmenu2 = false;
        _this.hmenu3 = false;
        _this.hmenu4 = false;
        _this.strUser = '';
        return _this;
    }
    HeaderComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.userService.getLogginUser().subscribe(function (data) {
            _this.appUser = data;
            _this.strUser = _this.appUser.fullName != null ? _this.appUser.fullName : _this.appUser.username;
            _this.storageService.save('userInfo', _this.appUser);
        }, function (error) {
            _this.alerts = _this.utils.handleError(error, _this.alerts);
        });
    };
    HeaderComponent.prototype.gotoHome = function () {
        var link = ['/'];
        this.router.navigate(link);
    };
    HeaderComponent.prototype.gotoMyProfile = function () {
        var link = [Config.URL_PROFILE.name];
        this.router.navigate(link);
    };
    HeaderComponent.prototype.gotoManagement = function () {
        var link = [Config.URL_MY_ORGS.name];
        this.router.navigate(link);
    };
    HeaderComponent.prototype.logout = function () {
        this.userService.logout(Config.APP_LOGOUT.apiEndpoint);
    };
    HeaderComponent.decorators = [
        { type: core_1.Component, args: [{
                    selector: 'page-header',
                    //  template: require('./header.html'),
                    templateUrl: './static/header.html'
                },] },
    ];
    /** @nocollapse */
    HeaderComponent.ctorParameters = function () { return [
        { type: one_user_service_1.OneUserService, },
        { type: utils_1.Utils, },
        { type: router_1.Router, },
        { type: local_storage_service_1.LocalStorageService, },
        { type: refdata_service_1.RefdataService, },
    ]; };
    return HeaderComponent;
}(base_component_1.BaseComponent));
exports.HeaderComponent = HeaderComponent;
//# sourceMappingURL=header.js.map