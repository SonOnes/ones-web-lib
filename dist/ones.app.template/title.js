"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var utils_1 = require("../one.utils/utils/common/utils");
var core_2 = require("@ngx-translate/core");
var one_obj_1 = require("../one.utils/utils/common/one.obj");
var broadcast_1 = require("../one.utils/event.listener/broadcast");
var ng2_toasty_1 = require("ng2-toasty");
var $ = require("jquery");
var base_component_1 = require("../ones.app.template/base/base.component");
var local_storage_service_1 = require("../one.utils/localforage/local-storage.service");
var refdata_service_1 = require("../one.utils/refdata/refdata.service");
var one_user_service_1 = require("../one.utils/utils/common/one.user.service");
var TitleComponent = (function (_super) {
    __extends(TitleComponent, _super);
    function TitleComponent(router, route, broadcast, utils, storageService, oneUserService, toastyService, translate, refdataService) {
        var _this = _super.call(this, storageService, utils, oneUserService, refdataService) || this;
        _this.router = router;
        _this.route = route;
        _this.broadcast = broadcast;
        _this.utils = utils;
        _this.storageService = storageService;
        _this.oneUserService = oneUserService;
        _this.toastyService = toastyService;
        _this.translate = translate;
        _this.refdataService = refdataService;
        _this.alerts = [];
        _this.message = new one_obj_1.OnesMessage();
        return _this;
    }
    TitleComponent.prototype.ngOnInit = function () {
        // tbd
    };
    TitleComponent.prototype.ngAfterViewInit = function () {
        $('.sidebar ul.nav-list li').click(function (event) {
            event.stopPropagation();
        });
        $('.sidebar ul.nav-list li').on('click', function () {
            var child = $(this).children().length;
            console.log('===== ' + child);
            if (3 > child) {
                $('.sidebar ul.nav-list li').removeClass('active');
                $(this).addClass('active');
                $(this).parent().parent().addClass('active');
                return;
            }
            else {
                if ($(this).hasClass('open')) {
                    $(this).removeClass('open');
                }
                else {
                    $(this).addClass('open');
                }
                ;
                return;
            }
        });
        $('#sidebar-toggle-icon').on('click', function () {
            if ($('#sidebar').hasClass('menu-min')) {
                $('#sidebar').removeClass('menu-min');
                $(this).removeClass('fa-angle-double-right');
                $(this).addClass('fa-angle-double-left');
                $('.sidebar ul.nav-list li').width('190px');
                $('.main-content').css('margin-left', '190px');
                $('.footer .footer-inner .footer-content').css('margin-left', '190px');
            }
            else {
                $('#sidebar').addClass('menu-min');
                $(this).removeClass('fa-angle-double-left');
                $(this).addClass('fa-angle-double-right');
                $('.sidebar ul.nav-list li').width('40px');
                $('.main-content').css('margin-left', '40px');
                $('.footer .footer-inner .footer-content').css('margin-left', '40px');
            }
            ;
        });
        $('#menu-toggler').on('click', function () {
            if ($('#sidebar').hasClass('display')) {
                $('#sidebar').removeClass('display');
            }
            else {
                $('#sidebar').addClass('display');
            }
        });
        if ($('#sidebar').hasClass('menu-min')) {
            $('.footer .footer-inner .footer-content').css('margin-left', '40px');
        }
        else {
            $('.footer .footer-inner .footer-content').css('margin-left', '190px');
        }
        /* check windows size*/
        var windowsize = $(window).width();
        $(window).resize(function () {
            windowsize = $(window).width();
            if (windowsize < 970) {
                $('.main-content').css('margin-left', '10px');
            }
            else {
                $('.main-content').css('margin-left', '190px');
            }
        });
    };
    TitleComponent.prototype.onClick = function ($event) {
        var target = event.target || event.srcElement || event.currentTarget;
        var tagName = $(target).parent().eq(0).prop('tagName');
        tagName = tagName.toLowerCase();
        var link = '';
        if ('a' === tagName) {
            link = $(target).parent().children().eq(2).val();
        }
        if ('li' === tagName) {
            link = $(target).parent().children().eq(0).children().eq(2).val();
        }
        if (null != link && undefined !== link && '' !== link) {
            this.gotoPage(link);
        }
    };
    TitleComponent.prototype.gotoPage = function (page) {
        var link = ['/' + page];
        this.router.navigate(link);
    };
    TitleComponent.prototype.closeAlert = function (i) {
        this.alerts.splice(i, 1);
    };
    TitleComponent.decorators = [
        { type: core_1.Component, args: [{
                    selector: 'page-title',
                    //  template: require('./title.html')
                    templateUrl: './static/title.html'
                },] },
    ];
    /** @nocollapse */
    TitleComponent.ctorParameters = function () { return [
        { type: router_1.Router, },
        { type: router_1.ActivatedRoute, },
        { type: broadcast_1.Broadcast, },
        { type: utils_1.Utils, },
        { type: local_storage_service_1.LocalStorageService, },
        { type: one_user_service_1.OneUserService, },
        { type: ng2_toasty_1.ToastyService, },
        { type: core_2.TranslateService, },
        { type: refdata_service_1.RefdataService, },
    ]; };
    return TitleComponent;
}(base_component_1.BaseComponent));
exports.TitleComponent = TitleComponent;
//# sourceMappingURL=title.js.map