import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {Response} from '@angular/http';
import * as _ from 'underscore';

import {WindowRefService} from '../one.utils/utils/common/window-ref.service';
import {OnesMessage} from '../one.utils/utils/common/one.obj';
import {Broadcast, EVENT} from '../one.utils/event.listener/broadcast';
import {OneUserService} from '../one.utils/utils/common/one.user.service';
import {BaseComponent} from './base/base.component';
import {LocalStorageService} from '../one.utils/localforage/local-storage.service';
import {Utils} from '../one.utils/utils/common/utils';
import {URL_SUCCESS} from '../config';
import {RefdataService} from '../one.utils/refdata/refdata.service';

import '../../style/login.scss';

@Component({
  selector: 'page-login',
//  template: require('./login.html')
  templateUrl: './static/login.html'
})
export class LoginComponent extends BaseComponent {
  private _window: Window;
  private username: string = null;
  private pass: string = null;
  private usernameValid: boolean = false;
  private passValid: boolean = false;
  public formNum: number;

  constructor(public userService: OneUserService, public storageService: LocalStorageService,
    protected utils: Utils, private broadcast: Broadcast, private router: Router,
    windowRef: WindowRefService, protected refdataService: RefdataService) {
    super(storageService, utils, userService, refdataService);
    this._window = windowRef.nativeWindow;
    this.formNum = 1;
  }

  ngOnInit(): void {
    // tbd
  }

  login(): void {
    this.broadcast.broadcast(EVENT.SHOW_SPINNER);
    var token: any;
    this.alerts = [];
    this.userService.login(this.username, this.pass).subscribe(data => {
      token = data;
      this.storageService.save('access_token', token.token_type + ' ' + token.access_token);
      this.storageService.save('refresh_token', token.token_type + ' ' + token.refresh_token);
      this.userService.getLogginUser().subscribe(data => {
        this.appUser = data;
        this.storageService.save('userInfo', this.appUser);
        this.router.navigate([URL_SUCCESS.name]);
      }, error => {
        this.alerts = this.utils.handleError(error, this.alerts);
      });
      this.broadcast.broadcast(EVENT.HIDE_SPINNER);
    }, error => {
      this.broadcast.broadcast(EVENT.HIDE_SPINNER);
      let message: OnesMessage = new OnesMessage();
      message.type = 'danger';
      if (error instanceof Response) {
        const body = error.json() || '';
        const err = body.error || JSON.stringify(body);
        console.log('err: ' + err);
        message.code = body.code;
        message.message = body.message;
      }
      this.alerts.push({msg: message.message, type: message.type, closable: true});
    });
  }
}
