import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Utils} from '../one.utils/utils/common/utils';
import {TranslateService} from '@ngx-translate/core';
import {OnesMessage} from '../one.utils/utils/common/one.obj';
import {Broadcast} from '../one.utils/event.listener/broadcast';
import {ToastyService, ToastOptions} from 'ng2-toasty';
import * as $ from 'jquery';

import {BaseComponent} from '../ones.app.template/base/base.component';
import {LocalStorageService} from '../one.utils/localforage/local-storage.service';
import {RefdataService} from '../one.utils/refdata/refdata.service';
import {OneUserService} from '../one.utils/utils/common/one.user.service';


@Component({
  selector: 'page-title',
//  template: require('./title.html')
  templateUrl: './static/title.html'
})
export class TitleComponent extends BaseComponent {
  public message: OnesMessage;
  public toastOption: ToastOptions;
  public formRebuildTitle: any;
  public formDownloadTitle: any;
  public toastRebuildMessageContent: any;
  public toastDownloadMessageContent: any;
  public alerts: Array<Object> = [];
  public downloadVAPUrl: string;
  public downloadAccountUrl: string;
  constructor(
    private router: Router, private route: ActivatedRoute, private broadcast: Broadcast,
    protected utils: Utils, public storageService: LocalStorageService,
    public oneUserService: OneUserService, private toastyService: ToastyService,
    private translate: TranslateService, protected refdataService: RefdataService
  ) {
    super(storageService, utils, oneUserService, refdataService);
    this.message = new OnesMessage();
  }

  ngOnInit(): void {
    // tbd
  }

  ngAfterViewInit(): void {
    $('.sidebar ul.nav-list li').click(function(event: any) {
      event.stopPropagation();
    });

    $('.sidebar ul.nav-list li').on('click', function() {
      var child = $(this).children().length;
      console.log('===== ' + child);
      if (3 > child) {
        $('.sidebar ul.nav-list li').removeClass('active');
        $(this).addClass('active');
        $(this).parent().parent().addClass('active');
        return;
      } else {
        if ($(this).hasClass('open')) {
          $(this).removeClass('open');
        } else {
          $(this).addClass('open');
        };
        return;
      }
    });

    $('#sidebar-toggle-icon').on('click', function() {
      if ($('#sidebar').hasClass('menu-min')) {
        $('#sidebar').removeClass('menu-min');
        $(this).removeClass('fa-angle-double-right');
        $(this).addClass('fa-angle-double-left');
        $('.sidebar ul.nav-list li').width('190px');
        $('.main-content').css('margin-left', '190px');
        $('.footer .footer-inner .footer-content').css('margin-left', '190px');
      } else {
        $('#sidebar').addClass('menu-min');
        $(this).removeClass('fa-angle-double-left');
        $(this).addClass('fa-angle-double-right');
        $('.sidebar ul.nav-list li').width('40px');
        $('.main-content').css('margin-left', '40px');
        $('.footer .footer-inner .footer-content').css('margin-left', '40px');
      };
    });

    $('#menu-toggler').on('click', function() {
      if ($('#sidebar').hasClass('display')) {
        $('#sidebar').removeClass('display');
      } else {
        $('#sidebar').addClass('display');
      }
    });
    if ($('#sidebar').hasClass('menu-min')) {
      $('.footer .footer-inner .footer-content').css('margin-left', '40px');
    } else {
      $('.footer .footer-inner .footer-content').css('margin-left', '190px');
    }

    /* check windows size*/
    var windowsize = $(window).width();
    $(window).resize(function() {
      windowsize = $(window).width();
      if (windowsize < 970) {
        $('.main-content').css('margin-left', '10px');
      } else {
        $('.main-content').css('margin-left', '190px');
      }
    });
  }

  onClick($event: any) {
    var target = event.target || event.srcElement || event.currentTarget;
    let tagName: string = $(target).parent().eq(0).prop('tagName');
    tagName = tagName.toLowerCase();
    let link: string = '';
    if ('a' === tagName) {
      link = $(target).parent().children().eq(2).val();
    }
    if ('li' === tagName) {
      link = $(target).parent().children().eq(0).children().eq(2).val();
    }
    if (null != link && undefined !== link && '' !== link) {
      this.gotoPage(link);
    }
  }
  gotoPage(page: string): void {
    let link = ['/' + page];
    this.router.navigate(link);
  }

  public closeAlert(i: number): void {
    this.alerts.splice(i, 1);
  }

}
