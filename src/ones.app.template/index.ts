import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpModule} from '@angular/http';
import {I18NModule} from '../one.utils/i18n/i18n.module';
import {FormsModule} from '@angular/forms';

import {HeaderComponent} from './header';
import {TitleComponent} from './title';
import {FooterComponent} from './footer';
import {SuccessComponent} from './success.component';
import {LoginComponent} from './login';
import {AboutComponent} from './about';
import {SpinnerComponent} from './spinner/spinner';
import {BasicDialogComponent} from './dialogs/basic';
import {ClickOutSiteDirective} from '../ones.app.template/directives/click.out.site';
import {AutofocusDirectives} from '../ones.app.template/directives/autofocus.directives';

import {OneUserService} from '../one.utils/utils/common/one.user.service';
import {Utils} from '../one.utils/utils/common/utils';
import {ToastyModule} from 'ng2-toasty';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    I18NModule,
    FormsModule,
    ToastyModule.forRoot(),
    NgbModule.forRoot()
  ],
  declarations: [
    AutofocusDirectives,
    ClickOutSiteDirective,
    HeaderComponent,
    TitleComponent,
    FooterComponent,
    SuccessComponent,
    LoginComponent,
    AboutComponent,
    SpinnerComponent,
    BasicDialogComponent
  ],
  exports: [
    AutofocusDirectives,
    ClickOutSiteDirective,
    HeaderComponent,
    TitleComponent,
    FooterComponent,
    SuccessComponent,
    LoginComponent,
    AboutComponent,
    SpinnerComponent,
    BasicDialogComponent
  ],
  providers: [
    OneUserService,
    Utils
  ],
})
export class TemplateOneAppModule {}
