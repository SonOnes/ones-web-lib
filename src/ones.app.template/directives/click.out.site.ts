import {Directive, HostListener, Input, ElementRef, Output, EventEmitter} from '@angular/core';

@Directive({
	selector: '[tohClickOutSite]'
})
export class ClickOutSiteDirective {
    @Output()
    public tohClickOutSite = new EventEmitter();

    constructor(private el: ElementRef) {
    }

	@HostListener('document:click', ['$event'])
	documentClick($event: any) {
        if (!this.el.nativeElement.contains($event.target)) {
            this.tohClickOutSite.emit(null);
        }
	}
}
