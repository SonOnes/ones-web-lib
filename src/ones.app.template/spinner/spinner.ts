import {Component, OnInit} from '@angular/core';
import {Broadcast, EVENT} from '../../one.utils/event.listener/broadcast';

@Component({
  selector: 'page-spinner',
  //	template: require('./spinner.html')
  templateUrl: './static/spinner.html'
})
export class SpinnerComponent implements OnInit {
  public isShow: boolean;
  constructor(private broadcast: Broadcast) {
  }

  ngOnInit(): void {
    this.broadcast.on<EVENT>(EVENT.SHOW_SPINNER)
      .subscribe(message => {
        this.isShow = true;
      });
    this.broadcast.on<EVENT>(EVENT.HIDE_SPINNER)
      .subscribe(message => {
        this.isShow = false;
      });
  }
}
