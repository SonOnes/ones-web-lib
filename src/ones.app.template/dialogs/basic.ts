import {Component, OnInit} from '@angular/core';
import {Broadcast, EVENT} from '../../one.utils/event.listener/broadcast';

@Component({
  selector: 'basic-dialog',
  //	template: require('./basic.html')
  templateUrl: './static/basic.html'
})
export class BasicDialogComponent implements OnInit {
  public isShowDialogConfirm: boolean;
  protected dialogObj: DialogObj;
  constructor(private broadcast: Broadcast) {
  }

  ngOnInit(): void {
    this.broadcast.on<EVENT>(EVENT.SHOW_CONFIRM_DIALOG)
      .subscribe(data => {
        this.dialogObj = data as any as DialogObj;
        this.isShowDialogConfirm = true;
      });
    this.broadcast.on<EVENT>(EVENT.HIDE_CONFIRM_DIALOG)
      .subscribe(data => {
        this.isShowDialogConfirm = false;
      });
  }

  cancel(): void {
    this.broadcast.broadcast(EVENT.HIDE_CONFIRM_DIALOG);
  }

  ok(): void {
    this.cancel();
    this.dialogObj.ok();
  }
}

export class DialogObj {
  message: string;
  title: string;
  okButtonLabel: string;
  cancelButtonLabel: string;
  clickOutSiteToClose: boolean;
  ok: (data?: any) => void;

  constructor(ok: (data?: any) => void) {
    this.okButtonLabel = 'Yes';
    this.cancelButtonLabel = 'No';
    this.title = 'Warning';
    this.message = 'Are you sure?';
    this.clickOutSiteToClose = true;
    this.ok = ok;
  }
}
