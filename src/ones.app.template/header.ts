import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import Config = require('../config');
import {Utils} from '../one.utils/utils/common/utils';
import {AppUser} from '../one.utils/utils/common/one.obj';
import {LocalStorageService} from '../one.utils/localforage/local-storage.service';
import {OneUserService} from '../one.utils/utils/common/one.user.service';
import {BaseComponent} from '../ones.app.template/base/base.component';
import {RefdataService} from '../one.utils/refdata/refdata.service';

@Component({
  selector: 'page-header',
  //  template: require('./header.html'),
  templateUrl: './static/header.html'
})
export class HeaderComponent extends BaseComponent {

  public hmenu1: boolean = false;
  public hmenu2: boolean = false;
  public hmenu3: boolean = false;
  public hmenu4: boolean = false;
  public strUser: string = '';

  constructor(protected userService: OneUserService, protected utils: Utils,
    private router: Router, public storageService: LocalStorageService, protected refdataService: RefdataService) {
    super(storageService, utils, userService, refdataService);
  }

  ngOnInit(): void {
    this.userService.getLogginUser().subscribe(data => {
      this.appUser = data;
      this.strUser = this.appUser.fullName != null ? this.appUser.fullName : this.appUser.username;
      this.storageService.save('userInfo', this.appUser);
    }, error => {
      this.alerts = this.utils.handleError(error, this.alerts);
    });
  }

  gotoHome(): void {
    let link = ['/'];
    this.router.navigate(link);
  }

  gotoMyProfile(): void {
    let link = [Config.URL_PROFILE.name];
    this.router.navigate(link);
  }

  gotoManagement(): void {
    let link = [Config.URL_MY_ORGS.name];
    this.router.navigate(link);
  }

  logout(): void {
    this.userService.logout(Config.APP_LOGOUT.apiEndpoint);
  }
}


