import { FormControl, FormGroup} from '@angular/forms';
import { Utils } from '../one.utils/utils/common/utils';
export class CustomValidators {
    static emailValidator(control: FormControl) {
        let regex = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if (control.value.match(regex)) {
            return null;
        } else {
            return { 'validEmail': true };
        }
    }

    static numberValidator(control: FormControl) {
        if (control.value >= 0 && control.value < 1000000000) {
            return null;
        } else {
            return { 'inValidNumber': true };
        }
    }

    static noLongerThan1000Validator(control: FormControl) {
        if (control.value >= 0 && control.value <= 1000) {
            return null;
        } else {
            return { 'inValidNumber1000': true };
        }
    }

    static noSpaceValidator(control: FormControl) {
        return (control.value && control.value.trim() == "") ? { "isSpace": true } : null;
    }

    static startDateEndDate(start: any, end: any) {
		let utils = new Utils();
		return (group: FormGroup): { [key: string]: any } => {
            let startDate = group.controls[start];
            let endDate = group.controls[end];

            if (startDate.value && endDate.value) {
                if ((new Date(utils.parseJVDate(startDate.value))) > (new Date(utils.parseJVDate(endDate.value)))) {
					return {
						validationError: true
					};
                } else {
                    return null;
                }
            }
        }
    }
}
