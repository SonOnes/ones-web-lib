import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {LocalStorageService} from '../one.utils/localforage/local-storage.service';
import {ROOT_URL} from '../config';
import {EVENT, Broadcast} from '../one.utils/event.listener/broadcast';
import {OneUserService} from '../one.utils/utils/common/one.user.service';
import {Utils} from '../one.utils/utils/common/utils';

@Component({
  template: ''
})
export class SuccessComponent implements OnInit {

  private alerts: Array<Object> = [];

  constructor(private storageService: LocalStorageService, private route: ActivatedRoute, private utils: Utils,
    private broadcast: Broadcast, public userService: OneUserService, private router: Router) {
  }

  ngOnInit(): void {
    this.broadcast.broadcast(EVENT.SHOW_SPINNER);
    let tokenType: string = '';
    let token: string = '';
    this.route.params.forEach((params: Params) => {
      tokenType = params['type'];
      token = params['token'];
    });
    if (null != tokenType && undefined !== tokenType) {
      this.storageService.save('access_token', tokenType + ' ' + token);
    }
    this.userService.getLogginUser().subscribe(data => {
        this.storageService.save('userInfo', data);
        this.gotoHome();
        this.broadcast.broadcast(EVENT.HIDE_SPINNER);
      }, error => {
          this.alerts = this.utils.handleError(error, this.alerts);
          this.broadcast.broadcast(EVENT.HIDE_SPINNER);
      });
  }

  gotoHome(): void {
    let auth: Object = this.storageService.get('url_before_auth');
    if (auth === undefined || auth === null || auth === '' || JSON.stringify(auth) === JSON.stringify({})) {
      let address = [ROOT_URL.name];
      this.router.navigate(address);
    } else {
      let link = [auth];
      this.router.navigate(link);
    }
    this.storageService.save('url_before_auth', '');
  }
}
