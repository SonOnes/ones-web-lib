import {Component, OnInit, ViewChild, ElementRef, AfterViewInit} from '@angular/core';
import {GridOptions} from 'ag-grid/main';

import {Config} from '../../config';
import {LocalStorageService} from '../../one.utils/localforage/local-storage.service';
import {OneUserService} from '../../one.utils/utils/common/one.user.service';
import {AppUser, Dropdown, DropdownList, DropdownListItem, OnesPaging} from '../../one.utils/utils/common/one.obj';
import {Utils} from '../../one.utils/utils/common/utils';
import {City} from '../../one.utils/refdata/city';
import {CityFilter} from '../../one.utils/refdata/city.filter';
import {Country} from '../../one.utils/refdata/country';
import {CountryFilter} from '../../one.utils/refdata/country.filter';
import {RefdataService} from '../../one.utils/refdata/refdata.service';
import {Base} from './base';

export abstract class BaseEditComponent extends Base {

  constructor(protected storageService: LocalStorageService, protected utils: Utils,
    protected oneUserService: OneUserService, protected refdataService: RefdataService) {
    super(storageService, utils, oneUserService, refdataService);
    this.provinceIdDropdown.settings = Config.singleDropdownSettings;
  }
}
