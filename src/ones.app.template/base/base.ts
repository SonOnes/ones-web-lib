import {Component, OnInit, ViewChild, ElementRef, AfterViewInit} from '@angular/core';
import {GridOptions} from 'ag-grid/main';
import * as moment from 'moment';
import {IMyDrpOptions, IMyDateRangeModel} from 'mydaterangepicker';

import {Config} from '../../config';
import {LocalStorageService} from '../../one.utils/localforage/local-storage.service';
import {OneUserService} from '../../one.utils/utils/common/one.user.service';
import {AppUser, Dropdown, DropdownList, DropdownListItem, OnesPaging} from '../../one.utils/utils/common/one.obj';
import {Utils} from '../../one.utils/utils/common/utils';
import {City} from '../../one.utils/refdata/city';
import {CityFilter} from '../../one.utils/refdata/city.filter';
import {Country} from '../../one.utils/refdata/country';
import {CountryFilter} from '../../one.utils/refdata/country.filter';
import {RefdataService} from '../../one.utils/refdata/refdata.service';

export abstract class Base implements OnInit, AfterViewInit {
  public static scountries: Country[] = [];
  protected countryIdDropdown: Dropdown = new Dropdown();
  protected provinceIdDropdown: Dropdown = new Dropdown();
  public appUser: AppUser;
  public alerts: Array<Object> = [];
  public dateFormat: string;
  protected countrySelected: number;
  protected countries: Country[] = [];
  protected el: ElementRef;
  protected boolDefs: DropdownListItem[] = [];
  protected myDateRangeOptions: IMyDrpOptions = {
    dateFormat: Config.dateRFormat,
  };
  constructor(protected storageService: LocalStorageService, protected utils: Utils,
    protected oneUserService: OneUserService, protected refdataService: RefdataService) {
    this.appUser = this.storageService.get('userInfo');
    this.dateFormat = Config.dateFormat;
    this.countryIdDropdown = new Dropdown();
    this.provinceIdDropdown = new Dropdown();
    this.countrySelected = Config.defaultCountry;
    this.countryIdDropdown.settings = Config.singleDropdownSettings;
    this.provinceIdDropdown.settings = Config.multiDropdownSettings;
  }

  ngOnInit(): void {
    // tbd
  }

  ngAfterViewInit(): void {
    // tbd
  }

  protected toUTCDate(date: any): any {
    var _utc = new Date(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate());
    return _utc;
  };

  protected millisToUTCDate(millis: any): any {
    if (millis) {
      return this.toUTCDate(new Date(millis));
    }
  };

  public checkRoles(key: string): boolean {
    return this.oneUserService.checkRoles(key, this.appUser);
  }

  protected checkAuthority(api: string, method: string): boolean {
    return this.oneUserService.checkAuthority(api, method, this.appUser);
  }

  public closeAlert(i: number): void {
    this.alerts.splice(i, 1);
  }

  public checkStrEmplty(str: string) {
    if (null == str || '' === str) {
      return true;
    }
    return false;
  }

  protected getCountries() {
    var cfilter: CountryFilter = new CountryFilter();
    cfilter.limit = 500;
    if (null == Base.scountries || 0 === Base.scountries.length) {
      this.refdataService.searchCounties(cfilter).subscribe(data => {
        let results: OnesPaging<Country> = new OnesPaging<Country>(data);
        Base.scountries = results.content;
        this.countries = Base.scountries;
        this.countryIdDropdown.listItem = new DropdownList(null, this.countries).items;
        this.countryIdDropdown.itemsSelected = this.utils.selectValue(this.countrySelected, this.countryIdDropdown.listItem);
      }, error => {
        this.alerts = this.utils.handleError(error, this.alerts);
      });
    } else {
      this.countries = Base.scountries;
      this.countryIdDropdown.listItem = new DropdownList(null, this.countries).items;
      this.countryIdDropdown.itemsSelected = this.utils.selectValue(this.countrySelected, this.countryIdDropdown.listItem);
    }
  }

  protected onSelectCountryId($event: any) {
    if (null == this.countryIdDropdown.itemsSelected || 0 === this.countryIdDropdown.itemsSelected.length) {
      this.provinceIdDropdown.listItem = [];
      this.provinceIdDropdown.itemsSelected = [];
      return;
    }
    var cfilter: CityFilter = new CityFilter();
    cfilter.limit = 500;
    cfilter.countryId = this.utils.parseSelectValues(this.countryIdDropdown.itemsSelected)[0];

    this.refdataService.searchCities(cfilter).subscribe(data => {
      let results: OnesPaging<City> = new OnesPaging<City>(data);
      this.provinceIdDropdown.listItem = new DropdownList(null, results.content).items;
    }, error => {
      this.alerts = this.utils.handleError(error, this.alerts);
    });
  }
  protected getCity() {
    var cfilter: CityFilter = new CityFilter();
    cfilter.limit = 500;
    cfilter.countryId = this.countrySelected;

    this.refdataService.searchCities(cfilter).subscribe(data => {
      let results: OnesPaging<City> = new OnesPaging<City>(data);
      this.provinceIdDropdown.listItem = new DropdownList(null, results.content).items;
    }, error => {
      this.alerts = this.utils.handleError(error, this.alerts);
    });
  }
  protected OnDeSelectCountryId($event: any) {
    this.provinceIdDropdown.itemsSelected = [];
  }

  protected formatGridDate(data: any) {
    let result = data.value;
    if ('' === result || null === result || undefined === result) {
      return '';
    }
    return moment(new Date(result)).format('' + Config.dateGFormat);
  };

  protected checkIconType(icon: string): number {
    if (null === icon || undefined === icon) {
      return 0;
    }
    if (icon.indexOf('fa-') === 0) {
      return 1;
    }
    if (icon.indexOf('glyphicon') === 0) {
      return 2;
    }
    return 0;
  }

  protected onFocusOut($event: any) {
    var target = $event.target || $event.srcElement || $event.currentTarget;
    target.click();
  }
}
