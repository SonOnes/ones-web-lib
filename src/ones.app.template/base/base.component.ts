import {Component, OnInit, ViewChild, ElementRef, AfterViewInit} from '@angular/core';
import {GridOptions} from 'ag-grid/main';

import {LocalStorageService} from '../../one.utils/localforage/local-storage.service';
import {OneUserService} from '../../one.utils/utils/common/one.user.service';
import {AppUser, Dropdown, DropdownList, DropdownListItem, OnesPaging} from '../../one.utils/utils/common/one.obj';
import {Utils} from '../../one.utils/utils/common/utils';
import {City} from '../../one.utils/refdata/city';
import {CityFilter} from '../../one.utils/refdata/city.filter';
import {Country} from '../../one.utils/refdata/country';
import {CountryFilter} from '../../one.utils/refdata/country.filter';
import {RefdataService} from '../../one.utils/refdata/refdata.service';
import {Base} from './base';

export abstract class BaseComponent extends Base {

  protected gridOptions: GridOptions;
  protected showGrid: boolean;
  protected rowData: any[];
  protected columnDefs: any[];
  protected gridApi: any;
  protected gridColumnApi: any;
  protected components: any;
  protected rowCount: string;
  protected reload: boolean = false;
  @ViewChild('fileInput') protected fileInput: ElementRef;

  constructor(protected storageService: LocalStorageService, protected utils: Utils,
    protected oneUserService: OneUserService, protected refdataService: RefdataService) {
    super(storageService, utils, oneUserService, refdataService);

    this.reload = false;
    this.rowData = [];
    this.columnDefs = [];
    this.boolDefs = [];
    this.boolDefs.push(new DropdownListItem(null, 'Both'));
    this.boolDefs.push(new DropdownListItem(true, 'True'));
    this.boolDefs.push(new DropdownListItem(false, 'False'));
    this.components = {
      loadingRenderer: function(params: any) {
        if (params.value !== undefined) {
          return params.value;
        } else {
          return '<img src="../../../images/spinner.gif">';
        }
      }
    };
  }

  protected import() {
    let event = new MouseEvent('click', {bubbles: false});
    this.fileInput.nativeElement.dispatchEvent(event);
  }
}
