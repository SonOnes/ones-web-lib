
export interface IApi {
  apiEndpoint: string;
  method: string;
};

export interface IPath {
    name: string;
};

export const Config = {
  sso: true,
  rootService: '',
  urlLogin: '',
  loginToken: '',
  loginType: 'application/x-www-form-urlencoded',
  MAX_RESULT: 100,
  defaultCountry: 1,
  dateFormat: 'dd-MM-yyyy HH:mm:ss',
  dateGFormat: 'dd-MM-yyyy',
  dateRFormat: 'dd-mm-yyyy',
  singleDropdownSettings: {
    singleSelection: true,
    text: 'Select',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    enableSearchFilter: true,
    classes: 'custom-class'
  },
  multiDropdownSettings: {
    singleSelection: false,
    text: 'Select',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    enableSearchFilter: true,
    classes: 'custom-class'
  },
  TOAST_MESSAGE_TIMEOUT: 4,
  toastTheme: 'bootstrap',
  importTemplateType: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
};

export let APP_LANG: IApi = {
  apiEndpoint: 'app/config/',
  method: 'GET'
};

/* common page url */
export const ROOT_URL: IPath = {
    name: ''
};

export const URL_LOGIN: IPath = {
    name: 'login'
};

export const URL_ABOUT: IPath = {
    name: 'about'
};

export const URL_SUCCESS: IPath = {
    name: 'success'
};

export const URL_SUCCESS_SSO: IPath = {
    name: 'success/:type/:token'
};

export const URL_PROFILE: IPath = {
    name: 'profile'
};

/* org url*/

export const URL_MY_ORGS: IPath = {
    name: 'myorgs'
};
export const URL_ORG_MGNT: IPath = {
    name: 'myorg/detail/:id'
};
export const URL_EDIT_ORG: IPath = {
    name: 'myorg/:id'
};
export const URL_CREATE_ORG: IPath = {
    name: 'myorg/create'
};

/* manage function and authorization */
export const URL_FUNCTIONS: IPath = {
    name: 'functions'
};
export const URL_EDIT_FUNCTION: IPath = {
    name: 'function/edit/:id'
};
export const URL_FUNCTION: IPath = {
    name: 'function/detail/:id'
};
export const URL_CREATE_FUNCTION: IPath = {
    name: 'function/create'
};
// authentication apis
export let LOGGING_USER: IApi = {
  apiEndpoint: '/api/usersso/userinf',
  method: 'GET'
};

export let APP_LOGIN: IApi = {
  apiEndpoint: '/api/nonauth/login',
  method: 'GET'
};

export let APP_LOGOUT: IApi = {
  apiEndpoint: '/logout',
  method: 'GET'
};

/* authorization api */
export const FUNCTION_GET_LIST: IApi = {
  apiEndpoint: '/api/functions',
  method: 'POST'
};
export const FUNCTION_GET_DETAIL: IApi = {
  apiEndpoint: '/api/function',
  method: 'GET'
};
export const FUNCTION_EDIT_OBJ: IApi = {
  apiEndpoint: '/api/function',
  method: 'POST'
};
export const FUNCTION_EXPORT: IApi = {
  apiEndpoint: '/api/functions/export',
  method: 'POST'
};
/* ref data api */
export let COUNTRIES_API: IApi = {
  apiEndpoint: '/api/countrys',
  method: 'POST'
};

export let CITIES_API: IApi = {
  apiEndpoint: '/api/citys',
  method: 'POST'
};

export let DISTRICT_API: IApi = {
  apiEndpoint: '/api/districts',
  method: 'GET'
};

/* management api */
// org
export const MY_ORG_GET_LIST: IApi = {
  apiEndpoint: '/api/myorgs',
  method: 'POST'
};

export const MY_ORGS_GET_DETAIL: IApi = {
  apiEndpoint: '/api/mgnt/organization',
  method: 'POST'
};

export const MY_ORGS_EDIT_OBJ: IApi = {
  apiEndpoint: '/api/mgnt/organization',
  method: 'POST'
};

export const MY_ORGS_DEL_OBJ: IApi = {
  apiEndpoint: '/api/mgnt/organization',
  method: 'POST'
};

export const MY_ORGS_EXPORT: IApi = {
  apiEndpoint: '/api/mgnt/organizations/export',
  method: 'POST'
};
// authority
export const AUTHORITY_GET_LIST: IApi = {
  apiEndpoint: '/api/mgnt/authoritys',
  method: 'POST'
};
export const AUTHORITY_GET_DETAIL: IApi = {
  apiEndpoint: '/api/mgnt/authority',
  method: 'GET'
};
export const AUTHORITY_DEL_OBJ: IApi = {
  apiEndpoint: '/api/mgnt/authority',
  method: 'DETETE'
};
export const AUTHORITY_DEL_OBJs: IApi = {
  apiEndpoint: '/api/mgnt/authoritys',
  method: 'DELETE'
};
export const AUTHORITY_EDIT_OBJ: IApi = {
  apiEndpoint: '/api/mgnt/authority',
  method: 'POST'
};

// user
export const USER_GET_LIST: IApi = {
  apiEndpoint: '/api/mgnt/users',
  method: 'POST'
};
export const USER_GET_DETAIL: IApi = {
  apiEndpoint: '/api/mgnt/user',
  method: 'GET'
};
export const USER_DEL_OBJ: IApi = {
  apiEndpoint: '/api/mgnt/user',
  method: 'DETETE'
};
export const USER_EDIT_OBJ: IApi = {
  apiEndpoint: '/api/mgnt/user',
  method: 'POST'
};
export const USERAUTHORITIES_GET_LIST: IApi = {
  apiEndpoint: '/api/mgnt/userAuthoritiess',
  method: 'POST'
};
export const USERAUTHORITIES_GET_DETAIL: IApi = {
  apiEndpoint: '/api/mgnt/userAuthorities',
  method: 'GET'
};
export const USERAUTHORITIES_DEL_OBJ: IApi = {
  apiEndpoint: '/api/mgnt/userAuthorities',
  method: 'DETETE'
};

export const USERAUTHORITIES_EDIT_OBJ: IApi = {
  apiEndpoint: '/api/mgnt/userAuthorities',
  method: 'POST'
};
export const USERINFORMATION_GET_LIST: IApi = {
  apiEndpoint: '/api/mgnt/userInformations',
  method: 'POST'
};
export const USERINFORMATION_GET_DETAIL: IApi = {
  apiEndpoint: '/api/mgnt/userInformation',
  method: 'GET'
};
export const USERINFORMATION_DEL_OBJ: IApi = {
  apiEndpoint: '/api/mgnt/userInformation',
  method: 'DETETE'
};
export const USERINFORMATION_DEL_OBJs: IApi = {
  apiEndpoint: '/api/mgnt/userInformations',
  method: 'DELETE'
};
export const USERINFORMATION_EDIT_OBJ: IApi = {
  apiEndpoint: '/api/mgnt/userInformation',
  method: 'POST'
};