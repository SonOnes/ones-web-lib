import {OnesSort} from '../utils/common/one.obj';

export class CityFilter {
    code: string;
    countryId: number;
    createdOn: string;
    id: number;
    minimumwage: number;
    modifiedOn: string;
    name: string;
    postcode: string;
    status: StatusEnum[];
    timezone: string;

	page: number;
    limit: number;
    sortDTOs: OnesSort[];
}

export const enum StatusEnum {
    ACTIVE,
    DEACTIVE,
}

