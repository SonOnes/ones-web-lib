import {OnesSort} from '../utils/common/one.obj';

export class DistrictFilter {
    cityId: number;
	code: string;
	createdOn: string;
	id: number;
	modifiedOn: string;
	name: string;
	postalCode: string;
	status: StatusEnum[];
  
    page: number;
    limit: number;
    sortDTOs: OnesSort[];
}

export const enum StatusEnum {
    ACTIVE,
    DEACTIVE,
}

