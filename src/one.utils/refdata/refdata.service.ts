import { Injectable }    from '@angular/core';
import { Response } from '@angular/http';
import { HttpRequestService } from '../utils/common/http.request.service';
import {Observable} from 'rxjs/Observable';
import {OnesPaging, OnesMessage} from '../utils/common/one.obj';

import OneConfig = require('../../config');

import {Country} from './country';
import {CountryFilter} from './country.filter';
import {City} from './city';
import {CityFilter} from './city.filter';
import {District} from './district';
import {DistrictFilter} from './district.filter';

@Injectable()
export class RefdataService {

	constructor(private http: HttpRequestService) {
	}

    public searchCounties(filter: CountryFilter): Observable<OnesPaging<Country>> {
		var result: Observable<OnesPaging<Country>> = this.http.post(OneConfig.COUNTRIES_API.apiEndpoint, JSON.stringify(filter))
               .map((res: Response) => res.json());
		return result;
	}

	public searchCities(filter: CityFilter): Observable<OnesPaging<City>> {
		var result: Observable<OnesPaging<City>> = this.http.post(OneConfig.CITIES_API.apiEndpoint, JSON.stringify(filter))
               .map((res: Response) => res.json());
		return result;
	}

	public searchDistricts(filter: DistrictFilter): Observable<OnesPaging<District>> {
		var result: Observable<OnesPaging<District>> = this.http.post(OneConfig.DISTRICT_API.apiEndpoint, JSON.stringify(filter))
               .map((res: Response) => res.json());
		return result;
	}
}

