
export class Country {
    code: string;
    createdOn: string;
    id: number;
    languageCode: string;
    modifiedOn: string;
    name: string;
    postcode: string;
    private status: CountryStatusEnum;
    timezone: string;
}

const enum CountryStatusEnum {
    ACTIVE,
    DEACTIVE,
}

