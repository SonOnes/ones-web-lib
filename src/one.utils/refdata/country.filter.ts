import {OnesSort} from '../utils/common/one.obj';

export class CountryFilter {
    code: string;
    createdOn: string;
    id: number;
    languageCode: string;
    modifiedOn: string;
    name: string;
    postcode: string;
    status: StatusEnum;
    timezone: string;

    page: number;
    limit: number;
    sortDTOs: OnesSort[];
}

export const enum StatusEnum {
    ACTIVE,
    DEACTIVE,
}

