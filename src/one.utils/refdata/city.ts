import {Country} from './country';

export class City {
    code: string;
    countryCountryId: Country;
    countryId: number;
    createdOn: string;
    id: number;
    minimumwage: number;
    modifiedOn: string;
    name: string;
    postcode: string;
    private status: CityStatusEnum;
    timezone: string;
}

const enum CityStatusEnum {
    ACTIVE,
    DEACTIVE,
}

