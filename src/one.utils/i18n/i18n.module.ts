import { NgModule, ModuleWithProviders } from '@angular/core';
import { Http } from '@angular/http';
import {TranslateModule, TranslateLoader, TranslateService} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import OneConfig = require('../../config');

export function createTranslateLoader(http: Http/*, config: Config*/) {
	// let root: string = config.getServiceRoot();
	// if (root.indexOf('localhost') !== -1) {
	// 	return new TranslateStaticLoader(http, 'sumo/mock', '.json');
	// }
    return new TranslateHttpLoader(http, OneConfig.APP_LANG.apiEndpoint, '');
}

@NgModule({
	imports: [
		TranslateModule.forRoot({
			loader: {
				provide: TranslateLoader,
				useFactory: createTranslateLoader,
				deps: [Http]
			}
		})
    ],
	declarations: [],
	exports: [TranslateModule]
})
export class I18NModule {

	static forRoot(): ModuleWithProviders {
		return {
			ngModule: I18NModule
		};
	}

    constructor(private translate: TranslateService/*, private config: Config*/) {
		// let root: string = config.getServiceRoot();
		let currentLang: string = 'language_en.json';
		let langs: Array<string> = ['language_en.json', 'language_vi.json'];
		// if (root.indexOf('localhost') !== -1) {
		// 	currentLang = 'en';
		// 	langs = ['en', 'es'];
		// }
		translate.addLangs(langs);
		translate.setDefaultLang(currentLang);
		translate.use(currentLang);
	}
}

export class MessageObj {
    key: string;
    value: string;
}
