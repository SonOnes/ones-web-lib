import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpModule} from '@angular/http';
import {FormsModule} from '@angular/forms';

import {I18NModule} from './i18n/i18n.module';
import {OneUserService} from './utils/common/one.user.service';
import {ExponentialStrengthPipe} from './filter';
import {ToastyModule} from 'ng2-toasty';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    I18NModule,
    FormsModule,
    ToastyModule.forRoot(),
    NgbModule.forRoot()
  ],
  declarations: [
    ExponentialStrengthPipe
  ],
  exports: [
    ExponentialStrengthPipe
  ],
  providers: [
    OneUserService,
  ],
})
export class OneUtilsModule {}
