import {Injectable, ElementRef} from '@angular/core';
import {Response} from '@angular/http';
import {HttpRequestService} from '../common/http.request.service';
import {Observable} from 'rxjs/Observable';
import {OnesPaging, OnesMessage} from '../common/one.obj';
import {Utils} from '../common/utils';

import {FUNCTION_GET_LIST, FUNCTION_GET_DETAIL, FUNCTION_EDIT_OBJ, FUNCTION_EXPORT, Config} from '../../../config';
import {Function} from './function';
import {FunctionFilter} from './function.filter';

@Injectable()
export class FunctionService {

  constructor(private http: HttpRequestService, private utils: Utils) {
  }

  public getMaxFirst(): Observable<Array<Function>> {
    let functionFilter: FunctionFilter = new FunctionFilter();
  	functionFilter.limit = Config.MAX_RESULT;
    var functions: Observable<Array<Function>> = this.http.post(FUNCTION_GET_LIST.apiEndpoint, JSON.stringify(functionFilter))
      .map((res: Response) => {
        let jsonObj = res.json();
        return jsonObj.content;
      });
    return functions;
  }

  public searchForTypeAhead(functionFilter: FunctionFilter): Observable<Array<Function>> {
    var functions: Observable<Array<Function>> = this.http.post(FUNCTION_GET_LIST.apiEndpoint, JSON.stringify(functionFilter))
      .map((res: Response) => {
        let jsonObj = res.json();
        return jsonObj.content;
      });
    return functions;
  }

  public search(functionFilter: FunctionFilter): Observable<OnesPaging<Function>> {
    var functions: Observable<OnesPaging<Function>> = this.http.post(FUNCTION_GET_LIST.apiEndpoint, JSON.stringify(functionFilter))
      .map((res: Response) => res.json());
    return functions;
  }

  public getObj(id: any): Observable<Function> {

    var query = '?';
    if (FUNCTION_GET_DETAIL.apiEndpoint.indexOf('?') > -1) {
      query = '&';
    }
    query += id;
    var functionDetail: Observable<Function> =  this.http.get(FUNCTION_GET_DETAIL.apiEndpoint + query)
      .map((res: Response) => res.json());
    return functionDetail;

  }

  public save(functionSave: Function): Observable<Function> {

    var functionDetail: Observable<Function> = this.http.post(FUNCTION_EDIT_OBJ.apiEndpoint, JSON.stringify(functionSave))
      .map((res: Response) => res.json());
    return functionDetail;

  }

  public export(filter: FunctionFilter): Observable<Blob> {
    var res: Observable<Blob> = this.http.postDownload(FUNCTION_EXPORT.apiEndpoint, JSON.stringify(filter)).map((res: Response) => {
      return new Blob([res.blob()], {type: Config.importTemplateType});
    });
    return res;
  }
}
