import {OnesSort} from '../common/ones.sort.obj';

export class AuthorityFilter {
    id: number;
    name: string;
    clientId: string;
    page: number;
    limit: number;

    sortDTOs: OnesSort[];
}


