import {OnesSort} from '../common/ones.sort.obj';

export class OrganizationFilter {
    id: number;
    code: string;
    name: string;
    createdFor: number;
    createdBy: number;
    createdOnFrom: string;
    createdOnTo: string;
    updatedBy: number;
    updatedOnFrom: string;
    updatedOnTo: string;
    parent: number;
    page: number;
    limit: number;
    options: any[];

    sortDTOs: OnesSort[];
}


export const KEY_NAME: string = 'onesuser.title.organization';
export const EXPORT_FIELDS: string[] = ['code', 'name', 'createdFor', 'createdOn', 'updatedOn', 'parent'];
export const TEMPLATE_FIELDS: string[] = ['code', 'name', 'createdFor', 'parent'];

