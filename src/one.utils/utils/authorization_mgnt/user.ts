

export class User {
  id: number = 0;
  firstName: string;
  lastName: string;
  username: string;
  password: string;
  createdOn: string;
  currentLogin: string;
  lastLogin: string;
  private status: UserStatusEnum;
  countryId: number;
  provinceId: number;
  provinceName: number;
  updatedOn: string;
  updatedBy: number;
  tel1: string;
  address: string;
  nickName: string;
  faceAccount: string;
  linkedAccount: string;
  githubAccount: string;
}

const enum UserStatusEnum {
  ACTIVE,
  DEACTIVE,
  PENDING
}

