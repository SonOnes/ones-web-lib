import {Injectable} from '@angular/core';
import {Response} from '@angular/http';
import {HttpRequestService} from '../common/http.request.service';
import {Observable} from 'rxjs/Observable';
import {OnesPaging} from '../common/paging.ones.obj';
import {OnesMessage} from '../common/ones.message';

import {USERAUTHORITIES_GET_LIST, USERAUTHORITIES_GET_DETAIL, USERAUTHORITIES_EDIT_OBJ, USERAUTHORITIES_DEL_OBJ, Config} from '../../../config';
import {UserAuthorities} from './user.authorities';
import {UserAuthoritiesFilter} from './user.authorities.filter';

@Injectable()
export class UserauthoritiesService {

  constructor(private http: HttpRequestService) {
  }

  public getFirst1000(): Observable<OnesPaging<UserAuthorities>> {
    let userAuthoritiesFilter: UserAuthoritiesFilter = new UserAuthoritiesFilter();
    userAuthoritiesFilter.limit = Config.MAX_RESULT;
    return this.search(userAuthoritiesFilter);
  }

  public search(userAuthoritiesFilter: UserAuthoritiesFilter): Observable<OnesPaging<UserAuthorities>> {
    var userAuthoritiess: Observable<OnesPaging<UserAuthorities>> = this.http.post(USERAUTHORITIES_GET_LIST.apiEndpoint, JSON.stringify(userAuthoritiesFilter))
      .map((res: Response) => res.json());
    return userAuthoritiess;
  }

  public deleteObj(id: any): Observable<OnesMessage> {

    var query = '?';
    if (USERAUTHORITIES_DEL_OBJ.apiEndpoint.indexOf('?') > -1) {
      query = '&';
    }
    query += id;
    var message: Observable<OnesMessage> = this.http.delete(USERAUTHORITIES_DEL_OBJ.apiEndpoint + query)
      .map((res: Response) => res.json());
    return message;

  }

  public getObj(id: any): Observable<UserAuthorities> {

    var query = '?';
    if (USERAUTHORITIES_GET_DETAIL.apiEndpoint.indexOf('?') > -1) {
      query = '&';
    }
    query += id;
    var userAuthorities: Observable<UserAuthorities> = this.http.get(USERAUTHORITIES_GET_DETAIL.apiEndpoint + query)
      .map((res: Response) => res.json());
    return userAuthorities;

  }

  public save(userAuthoritiesSave: UserAuthorities): Observable<UserAuthorities> {

    var userAuthorities: Observable<UserAuthorities> = this.http.post(USERAUTHORITIES_EDIT_OBJ.apiEndpoint, JSON.stringify(userAuthoritiesSave))
      .map((res: Response) => res.json());
    return userAuthorities;

  }
}
