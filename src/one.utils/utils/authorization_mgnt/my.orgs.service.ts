import {Injectable} from '@angular/core';
import {Response} from '@angular/http';
import {HttpRequestService} from '../common/http.request.service';
import {Observable} from 'rxjs/Observable';
import {OnesPaging} from '../common/paging.ones.obj';
import {OnesMessage} from '../common/ones.message';
import {Utils} from '../common/utils';

import {MY_ORG_GET_LIST, MY_ORGS_GET_DETAIL, MY_ORGS_EDIT_OBJ, MY_ORGS_DEL_OBJ, MY_ORGS_EXPORT, Config} from '../../../config';
import {Organization} from './organization';
import {OrganizationFilter} from './organization.filter';

@Injectable()
export class MyOrgsService {
  constructor(private http: HttpRequestService, private utils: Utils) {
  }

  public searchForTypeAhead(organizationFilter: OrganizationFilter): Observable<Array<Organization>> {
    var organizations: Observable<Array<Organization>> = this.http.post(MY_ORG_GET_LIST.apiEndpoint, JSON.stringify(organizationFilter))
      .map((res: Response) => {
        let jsonObj = res.json();
        return jsonObj.content;
      });
    return organizations;
  }

  public search(organizationFilter: OrganizationFilter): Observable<OnesPaging<Organization>> {
    var organizations: Observable<OnesPaging<Organization>> = this.http.post(MY_ORG_GET_LIST.apiEndpoint, JSON.stringify(organizationFilter))
      .map((res: Response) => res.json());
    return organizations;
  }

  public deleteObj(id: any): Observable<OnesMessage> {

    var query = '?';
    if (MY_ORGS_DEL_OBJ.apiEndpoint.indexOf('?') > -1) {
      query = '&';
    }
    query += id;
    var message: Observable<OnesMessage> = this.http.delete(MY_ORGS_DEL_OBJ.apiEndpoint + query)
      .map((res: Response) => res.json());
    return message;
  }

  public getObj(id: any): Observable<Organization> {

    var query = '?';
    if (MY_ORGS_GET_DETAIL.apiEndpoint.indexOf('?') > -1) {
      query = '&';
    }
    query += id;
    var organizationDetail: Observable<Organization> = this.http.get(MY_ORGS_GET_DETAIL.apiEndpoint + query)
      .map((res: Response) => res.json());
    return organizationDetail;

  }

  public save(organizationSave: Organization): Observable<Organization> {

    var organizationDetail: Observable<Organization> = this.http.post(MY_ORGS_EDIT_OBJ.apiEndpoint, JSON.stringify(organizationSave))
      .map((res: Response) => res.json());
    return organizationDetail;
  }

  public export(filter: OrganizationFilter): Observable<Blob> {
    var res: Observable<Blob> = this.http.postDownload(MY_ORGS_EXPORT.apiEndpoint, JSON.stringify(filter)).map((res: Response) => {
      return new Blob([res.blob()], {type: Config.importTemplateType});
    });
    return res;
  }
}
