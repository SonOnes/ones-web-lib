import { Injectable }    from '@angular/core';
import { Response } from '@angular/http';
import { HttpRequestService } from '../common/http.request.service';
import {Observable} from 'rxjs/Observable';
import { OnesPaging } from '../common/paging.ones.obj';
import {OnesMessage} from '../common/ones.message';

import { USERINFORMATION_GET_LIST, USERINFORMATION_GET_DETAIL, USERINFORMATION_EDIT_OBJ, USERINFORMATION_DEL_OBJ, USERINFORMATION_DEL_OBJs, Config }  from '../../../config';
import { UserInformation } from './user.information';
import { UserInformationFilter } from './user.information.filter';

@Injectable()
export class UserinformationService {

  constructor(private http: HttpRequestService) {
  }

  public getFirst1000(): Observable<OnesPaging<UserInformation>> {
	  let userInformationFilter: UserInformationFilter = new UserInformationFilter();
  	  userInformationFilter.limit = Config.MAX_RESULT;
	  return this.search(userInformationFilter);
  }

  public search(userInformationFilter: UserInformationFilter): Observable<OnesPaging<UserInformation>> {
    var userInformations: Observable<OnesPaging<UserInformation>> = this.http.post(USERINFORMATION_GET_LIST.apiEndpoint, JSON.stringify(userInformationFilter))
               .map((res: Response) => res.json());
    return userInformations;
  }

  public deleteObj(id: any): Observable<OnesMessage> {

    var query = '?';
    if (USERINFORMATION_DEL_OBJ.apiEndpoint.indexOf('?') > -1) {
        query = '&';
    }
    query += id;
    var message: Observable<OnesMessage> =  this.http.delete(USERINFORMATION_DEL_OBJ.apiEndpoint + query)
               .map((res: Response) => res.json());
    return message;

  }

  public deleteObjs(id: any): Observable<OnesMessage> {

	      var query = '?';
	      if (USERINFORMATION_DEL_OBJs.apiEndpoint.indexOf('?') > -1) {
	          query = '&';
	      }
	      query += id;
	      var message: Observable<OnesMessage>;
	      return message;

	    }

  public getObj(id: any): Observable<UserInformation> {

    var query = '?';
    if (USERINFORMATION_GET_DETAIL.apiEndpoint.indexOf('?') > -1) {
        query = '&';
    }
    query += id;
    var userInformation: Observable<UserInformation> =  this.http.get(USERINFORMATION_GET_DETAIL.apiEndpoint + query)
               .map((res: Response) => res.json());
    return userInformation;

  }

  public save(userInformationSave: UserInformation): Observable<UserInformation> {

    var userInformation: Observable<UserInformation> = this.http.post(USERINFORMATION_EDIT_OBJ.apiEndpoint, JSON.stringify(userInformationSave))
               .map((res: Response) => res.json());
    return userInformation;

  }
}
