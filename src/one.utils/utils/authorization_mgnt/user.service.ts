import {Injectable} from '@angular/core';
import {Response} from '@angular/http';
import {HttpRequestService} from '../common/http.request.service';
import {Observable} from 'rxjs/Observable';
import {OnesPaging} from '../common/paging.ones.obj';
import {OnesMessage} from '../common/ones.message';

import {USER_GET_LIST, USER_GET_DETAIL, USER_EDIT_OBJ, USER_DEL_OBJ, Config} from '../../../config';
import {User} from './user';
import {UserFilter} from './user.filter';

@Injectable()
export class UserService {

  constructor(private http: HttpRequestService) {
  }

  public getFirst1000(): Observable<OnesPaging<User>> {
    let userFilter: UserFilter = new UserFilter();
    userFilter.limit = Config.MAX_RESULT;
    return this.search(userFilter);
  }

  public search(userFilter: UserFilter): Observable<OnesPaging<User>> {
    var users: Observable<OnesPaging<User>> = this.http.post(USER_GET_LIST.apiEndpoint, JSON.stringify(userFilter))
      .map((res: Response) => res.json());
    return users;
  }

  public searchForTypeAhead(userFilter: UserFilter): Observable<Array<User>> {
    var organizations: Observable<Array<User>> = this.http.post(USER_GET_LIST.apiEndpoint, JSON.stringify(userFilter))
      .map((res: Response) => {
        let jsonObj = res.json();
        return jsonObj.content;
      });
    return organizations;
  }

  public deleteObj(id: any): Observable<OnesMessage> {

    var query = '?';
    if (USER_DEL_OBJ.apiEndpoint.indexOf('?') > -1) {
      query = '&';
    }
    query += id;
    var message: Observable<OnesMessage> = this.http.delete(USER_DEL_OBJ.apiEndpoint + query)
      .map((res: Response) => res.json());
    return message;

  }

  public getObj(id: any): Observable<User> {

    var query = '?';
    if (USER_GET_DETAIL.apiEndpoint.indexOf('?') > -1) {
      query = '&';
    }
    query += id;
    var user: Observable<User> = this.http.get(USER_GET_DETAIL.apiEndpoint + query)
      .map((res: Response) => res.json());
    return user;

  }

  public save(userSave: User): Observable<User> {

    var user: Observable<User> = this.http.post(USER_EDIT_OBJ.apiEndpoint, JSON.stringify(userSave))
      .map((res: Response) => res.json());
    return user;

  }
}
