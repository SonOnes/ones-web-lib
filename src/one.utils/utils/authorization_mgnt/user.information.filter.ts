import {OnesSort} from '../common/ones.sort.obj';

export class UserInformationFilter {
    id: number;
    userId: number;
    verifyToken: string;
    verifiedOnFrom: string;
    verifiedOnTo: string;
    page: number;
    limit: number;

    sortDTOs: OnesSort[];
}


