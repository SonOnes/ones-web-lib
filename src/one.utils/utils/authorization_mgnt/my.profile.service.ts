import {Injectable} from '@angular/core';
import {Response} from '@angular/http';
import {HttpRequestService} from '../common/http.request.service';
import {Observable} from 'rxjs/Observable';
import {OnesPaging} from '../common/paging.ones.obj';
import {OnesMessage} from '../common/ones.message';
import {Utils} from '../common/utils';

import {LOGGING_USER, USER_EDIT_OBJ, Config} from '../../../config';
import {Organization} from './organization';
import {OrganizationFilter} from './organization.filter';

@Injectable()
export class MyProfileService {
  constructor(private http: HttpRequestService, private utils: Utils) {
  }

  public getObj(): Observable<Organization> {
    var organizationDetail: Observable<Organization> = this.http.get(LOGGING_USER.apiEndpoint)
      .map((res: Response) => res.json());
    return organizationDetail;

  }

  public save(organizationSave: Organization): Observable<Organization> {

    var organizationDetail: Observable<Organization> = this.http.post(USER_EDIT_OBJ.apiEndpoint, JSON.stringify(organizationSave))
      .map((res: Response) => res.json());
    return organizationDetail;
  }
}
