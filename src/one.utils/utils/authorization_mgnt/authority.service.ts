import { Injectable }    from '@angular/core';
import { Response } from '@angular/http';
import { HttpRequestService } from '../common/http.request.service';
import {Observable} from 'rxjs/Observable';
import { OnesPaging } from '../common/paging.ones.obj';
import {OnesMessage} from '../common/ones.message';

import { AUTHORITY_GET_LIST, AUTHORITY_GET_DETAIL, AUTHORITY_EDIT_OBJ, AUTHORITY_DEL_OBJ, AUTHORITY_DEL_OBJs, Config }  from '../../../config';
import { Authority } from './authority';
import { AuthorityFilter } from './authority.filter';

@Injectable()
export class AuthorityService {

  constructor(private http: HttpRequestService) {
  }

  public getFirst1000(): Observable<OnesPaging<Authority>> {
	  let authorityFilter: AuthorityFilter = new AuthorityFilter();
  	  authorityFilter.limit = Config.MAX_RESULT;
	  return this.search(authorityFilter);
  }

  public search(authorityFilter: AuthorityFilter): Observable<OnesPaging<Authority>> {
    var authoritys: Observable<OnesPaging<Authority>> = this.http.post(AUTHORITY_GET_LIST.apiEndpoint, JSON.stringify(authorityFilter))
               .map((res: Response) => res.json());
    return authoritys;
  }

  public deleteObj(id: any): Observable<OnesMessage> {

    var query = '?';
    if (AUTHORITY_DEL_OBJ.apiEndpoint.indexOf('?') > -1) {
        query = '&';
    }
    query += id;
    var message: Observable<OnesMessage> =  this.http.delete(AUTHORITY_DEL_OBJ.apiEndpoint + query)
               .map((res: Response) => res.json());
    return message;

  }

  public deleteObjs(id: any): Observable<OnesMessage> {

	      var query = '?';
	      if (AUTHORITY_DEL_OBJs.apiEndpoint.indexOf('?') > -1) {
	          query = '&';
	      }
	      query += id;
	      var message: Observable<OnesMessage>;
	      return message;

	    }

  public getObj(id: any): Observable<Authority> {

    var query = '?';
    if (AUTHORITY_GET_DETAIL.apiEndpoint.indexOf('?') > -1) {
        query = '&';
    }
    query += id;
    var authority: Observable<Authority> =  this.http.get(AUTHORITY_GET_DETAIL.apiEndpoint + query)
               .map((res: Response) => res.json());
    return authority;

  }

  public save(authoritySave: Authority): Observable<Authority> {

    var authority: Observable<Authority> = this.http.post(AUTHORITY_EDIT_OBJ.apiEndpoint, JSON.stringify(authoritySave))
               .map((res: Response) => res.json());
    return authority;

  }
}
