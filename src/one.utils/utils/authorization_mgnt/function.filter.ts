import {OnesSort} from '../common/one.obj';

export class FunctionFilter {
    id: number;
    name: string;
    functionKey: string;
    api: string;
    method: string;
    allowAll: boolean;
    onOrg: boolean;
    onObj: boolean;
    icon: string;
    parent: number;
    page: number;
    limit: number;
    options: any[];

    sortDTOs: OnesSort[];
}


export const KEY_NAME: string = 'weddyvn.title.function';
export const EXPORT_FIELDS: string[] = ['name', 'functionKey', 'api', 'method', 'allowAll', 'onOrg', 'onObj', 'icon'];
export const TEMPLATE_FIELDS: string[] = ['name', 'functionKey', 'api', 'method', 'allowAll', 'onOrg', 'onObj', 'icon'];

