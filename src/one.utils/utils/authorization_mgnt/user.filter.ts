import {OnesSort} from '../common/ones.sort.obj';

export class UserFilter {
  id: number;
  firstName: string;
  lastName: string;
  username: string;
  password: string;
  createdOnFrom: string;
  createdOnTo: string;
  currentLoginFrom: string;
  currentLoginTo: string;
  lastLoginFrom: string;
  lastLoginTo: string;
  status: StatusEnum[];
  countryId: number;
  provinceId: number[];
  updatedOnFrom: string;
  updatedOnTo: string;
  updatedBy: number;
  tel1: string;
  address: string;
  nickName: string;
  page: number;
  limit: number;

  sortDTOs: OnesSort[];
}

export const enum StatusEnum {
  ACTIVE,
  DEACTIVE,
  PENDING
}

