

export class Function {
    id: number;
    name: string;
    functionKey: string;
    api: string;
    method: string;
    allowAll: boolean;
    onOrg: boolean;
    onObj: boolean;
    icon: string;
    parent: number;
}


