import {DropdownListItem} from './dropdown.list.item';

export class DropdownList {
  public items: DropdownListItem[] = [];
  constructor(strEnum: string[], objs: any[]) {

    if (null != strEnum && 0 < strEnum.length) {
      for (let str of strEnum) {
        this.items.push(new DropdownListItem(str, str));
      }
      return;
    }
    if (null != objs && 0 < objs.length) {
      for (let obj of objs) {
        this.items.push(new DropdownListItem(obj.id, obj.name));
      }
    }
  }

  public toValue(items: DropdownListItem[]) {
    let results: any[] = [];
    if (null == items) {
      return results;
    }
    for (let item of items) {
      results.push(item.id);
    }
    return results;
  }
}
