import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Http, ConnectionBackend, RequestOptions, RequestOptionsArgs, Request, Response, Headers, ResponseContentType} from '@angular/http';

import {LocalStorageService} from '../../localforage/local-storage.service';
import AppConfig = require('../../../config');

@Injectable()
export class HttpRequestService extends Http {

  private storageService: LocalStorageService = new LocalStorageService();

  constructor(backend: ConnectionBackend, defaultOptions: RequestOptions, private _router: Router) {
    super(backend, defaultOptions);
  }

  request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
    return this.intercept(super.request(url, options));
  }

  get(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.intercept(super.get(this.fullPath(url.toString()), this.getRequestOptionArgs(options)));
  }

  post(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.intercept(super.post(this.fullPath(url.toString()), body, this.getRequestOptionArgs(options)));
  }

  put(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.intercept(super.put(this.fullPath(url.toString()), body, this.getRequestOptionArgs(options)));
  }

  delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.intercept(super.delete(this.fullPath(url.toString()), this.getRequestOptionArgs(options)));
  }

  getDownload(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.intercept(super.get(this.fullPath(url.toString()), this.getDownloadRequestOptionArgs(options)));
  }

  postDownload(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.intercept(super.post(this.fullPath(url.toString()), body, this.getDownloadRequestOptionArgs(options)));
  }

  postUpload(url: string, file: any, options?: RequestOptionsArgs): Observable<Response> {
    var formData = new FormData();
    formData.append('file', file);
    return this.intercept(super.post(this.fullPath(url.toString()), formData, this.postUploadRequestOptionArgs(options)));
  }

  getRequestOptionArgs(options?: RequestOptionsArgs): RequestOptionsArgs {
    if (options == null) {
      options = new RequestOptions();
    }
    if (options.headers == null) {
      options.headers = new Headers();
      options.headers.append('Authorization', this.storageService.get('access_token'));
      options.headers.append('Content-Type', 'application/json;charset=UTF-8');
    }
    options.withCredentials = true;
    return options;
  }

  getDownloadRequestOptionArgs(options?: RequestOptionsArgs): RequestOptionsArgs {
    if (options == null) {
      options = new RequestOptions();
    }
    if (options.headers == null) {
      options.headers = new Headers();
      options.headers.append('Authorization', this.storageService.get('access_token'));
      options.headers.append('Content-Type', 'application/json;charset=UTF-8');
    }
    options.withCredentials = true;
    options.responseType = ResponseContentType.Blob;
    return options;
  }

  postUploadRequestOptionArgs(options?: RequestOptionsArgs): RequestOptionsArgs {
    if (options === null) {
      options = new RequestOptions();
    } else {
      return options;
    }
    if (options.headers === null) {
      options.headers = new Headers();
      options.headers.append('Authorization', this.storageService.get('access_token'));
      options.headers.append('Content-Type', 'application/json;charset=UTF-8');
    }
    options.withCredentials = true;
    options.responseType = ResponseContentType.Blob;
    return options;
  }

  getUploadRequestOptionArgs(resType: ResponseContentType, reqContentType: string): RequestOptionsArgs {
    let options: RequestOptionsArgs = new RequestOptions();
    if (options.headers == null) {
      options.headers = new Headers();
      options.headers.append('Authorization', this.storageService.get('access_token'));
      if (null != reqContentType) {
        options.headers.append('Content-Type', reqContentType);
      }
    }
    options.withCredentials = true;
    options.responseType = resType;
    return options;
  }

  private fullPath(path: string): string {
    return AppConfig.Config.rootService + path;
  }

  private intercept(observable: Observable<Response>): Observable<Response> {
    return observable.catch((err, source) => {
      if (err.status === 401) {
        const body = JSON.parse(err._body);
        let message: string = body['message'];
        if (AppConfig.Config.sso) {
          this.storageService.save('userInfo', '');
          window.location.href = AppConfig.Config.rootService + AppConfig.Config.urlLogin;
        } else if ('UNAUTHORIZED' === message || undefined === message) {
          this.storageService.save('userInfo', '');
          this._router.navigate([AppConfig.URL_LOGIN.name]);
        } else {
          window.location.href = message;
        }
        return Observable.empty();
      } else {
        return Observable.throw(err);
      }
    });
  }
}
