import { Injectable }    from '@angular/core';

@Injectable()
export class ConfigurationConstants {
  private _cookieName:string = 'etl_language';

  getCookieName(): string {
    return this._cookieName;
  }
}
/*
export var CONSTANTS_PROVIDERS:Provider[] = [
  provide(ConfigurationConstants, {useClass: ConfigurationConstants})
];*/
