
export class OnesPaging<T> {
    protected first: boolean;
    protected last: boolean;
    protected number: number;
    protected numberOfElements: number;
    protected size: number;
    // sort: any;
    public totalElements: number;
    private totalPages: number;
    public content: T[];

    constructor(data: any) {
        this.first = data.first;
        this.last = data.last;
        this.number = data.number;
        this.numberOfElements = data.numberOfElements;
        this.size = data.size;
        this.totalElements = data.totalElements;
        this.totalPages = data.totalPages;
        this.content = data.content;
    }
}
