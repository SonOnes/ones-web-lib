
export class Typeahead {
  public searching: boolean = false;
  public searchNoData: boolean = false;
  public inputSuggestFormatter: any;
  public formatterSuggestResult: any;
  public suggestion: any;
  public model: any;
}
