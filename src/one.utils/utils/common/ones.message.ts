export class OnesMessage {
    code: string;
    message: any;
    type: string;
    violations: any[];
}
