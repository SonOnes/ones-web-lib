export class DropdownListItem {
  public id: any;
  public itemName: String;
  constructor(id: any, itemName: string) {
    this.id = id;
    this.itemName = itemName;
  }
}
