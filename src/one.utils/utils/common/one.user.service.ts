import {Injectable} from '@angular/core';
import {Response} from '@angular/http';
import {RequestOptions, RequestOptionsArgs, Headers} from '@angular/http';
import {Router} from '@angular/router';

import {LocalStorageService} from '../../localforage/local-storage.service';
import {HttpRequestService} from '../common/http.request.service';
import {Observable} from 'rxjs/Observable';
import {AppUser} from './one.obj';
import OneConfig = require('../../../config');

@Injectable()
export class OneUserService {

  constructor(private http: HttpRequestService, private router: Router,
    public storageService: LocalStorageService) {
  }

  public getLogginUser(): Observable<AppUser> {
    var user: Observable<AppUser> = this.http.get(OneConfig.LOGGING_USER.apiEndpoint).map((res: Response) => res.json());
    return user;
  }

  public checkRoles(key: string, user: AppUser): boolean {
    if (user !== undefined && user !== null && user.roles !== undefined && user.roles.length > 0) {
      for (let obj in user.roles) {
        if (user.roles[obj].toUpperCase() === key.toUpperCase()) {
          return true;
        }
      }
    }
    return false;
  }

  public checkAuthority(key: string, method: string, user: AppUser): boolean {
    if (user !== undefined && user !== null && user.roles !== undefined && user.roles.length > 0) {
      for (let obj in user.apiFunctions) {
        let func: any = user.apiFunctions[obj];
        if (func.allowAll ||
          (func.api === key && func.method === method)) {
          return true;
        }
      }
    }
    return false;
  }

  public login(username: string, pass: string): Observable<any> {
    let options: RequestOptionsArgs = new RequestOptions();
    options.headers = new Headers();
    options.headers.append('Authorization', OneConfig.Config.loginToken);
    options.headers.append('Content-Type', OneConfig.Config.loginType);

    let token: Observable<any> = this.http.post(OneConfig.APP_LOGIN.apiEndpoint, 'grant_type=password&username=' + username + '&password=' + pass + '', options)
      .map((res: Response) => res.json());
    return token;
  }

  public logout(loginUrl: string): void {
    this.http.get(OneConfig.APP_LOGOUT.apiEndpoint).subscribe(data => {
      console.log('logout success');
    }, error => {console.log('logout error');});
    this.storageService.save('userInfo', '');
    this.storageService.save('access_token', '');
    this.storageService.save('refresh_token', '');
    this.router.navigateByUrl('/' + loginUrl);
  }
}
