import {NgbDateStruct, NgbDateParserFormatter} from '@ng-bootstrap/ng-bootstrap';

export class NgbDateObjParserFormatter extends NgbDateParserFormatter {
    constructor(private momentFormat: string) {
        super();
    };

    format(date: NgbDateStruct): string {
        if (date === null) {
            return '';
        }
        return ('0' + date.day + '').slice(-2) + '-' + ('0' + date.month + '').slice(-2) + '-' + date.year + '';
    }

    parse(value: string): NgbDateStruct {
		if (value) {
            console.log(value);
			const dateParts = value.trim().split('-');
			if (dateParts.length === 1 && isNumber(dateParts[0])) {
				return { year: toInteger(dateParts[0]), month: null, day: null };
			} else if (dateParts.length === 2 && isNumber(dateParts[0]) && isNumber(dateParts[1])) {
				return { year: toInteger(dateParts[1]), month: toInteger(dateParts[0]), day: null };
			} else if (dateParts.length === 3 && isNumber(dateParts[0]) && isNumber(dateParts[1]) && isNumber(dateParts[2])) {
				return { year: toInteger(dateParts[2]), month: toInteger(dateParts[1]), day: toInteger(dateParts[0]) };
			}
		}
		return null;
    }
}

export function toInteger(value: any) {
    return parseInt('' + value, 10);
}
export function toString(value: any) {
    return (value !== undefined && value !== null) ? '' + value : '';
}
export function getValueInRange(value: any, max: any, min: any) {
    if (min === void 0) { min = 0; }
    return Math.max(Math.min(value, max), min);
}
export function isString(value: any) {
    return typeof value === 'string';
}
export function isNumber(value: any) {
    return !isNaN(toInteger(value));
}
export function isDefined(value: any) {
    return value !== undefined && value !== null;
}
export function padNumber(value: any) {
    if (isNumber(value)) {
        return ('0' + value).slice(-2);
    } else {
        return '';
    }
}
