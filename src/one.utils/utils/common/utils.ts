import {Injectable} from '@angular/core';
import {Headers, Response} from '@angular/http';
import {OnesMessage} from './ones.message';
import {OnesSort} from './ones.sort.obj';
import {Config} from '../../../config';
import {DropdownListItem} from './dropdown.list.item';
import {FieldObj} from './field.obj';
import {ToastOptions, ToastData} from 'ng2-toasty';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {Observable} from 'rxjs';
import {UUID} from 'angular2-uuid';

@Injectable()
export class Utils {

  public isValid(obj: any): boolean {
    return obj !== null && obj !== undefined;
  }

  public handleError(error: any, alerts: Array<Object>): Array<Object> {
    var message: OnesMessage = new OnesMessage();
    message.type = 'danger';
    alerts = [];
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      message.code = body.code;
      message.message = body.message;
      message.violations = body.violations;
      if (401 === error.status) {
        window.location.href = Config.rootService + body.message;
      } else if (0 === error.status) {
        message.message = 'Cannot connect to server';
      }
    }
    alerts.push({msg: message.message, type: message.type, closable: true, violations: message.violations});
    return alerts;
  }

  public handleSuccess(success: OnesMessage, alerts: Array<Object>): Array<Object> {
    var message: OnesMessage = new OnesMessage();
    message.type = 'success';
    message.message = success.code;
    alerts = [];
    alerts.push({msg: message.message, type: message.type, closable: true});
    return alerts;
  }

  public getSorts(sorts: any[]): OnesSort[] {
    let sortDTOs: OnesSort[] = [];

    if (undefined !== sorts && undefined !== sorts[0]) {
      let sortDirection = 'ASC';
      if (sorts[0].sort !== 'asc') {
        sortDirection = 'DESC';
      } else {
        sortDirection = 'ASC';
      }
      sortDTOs.push({property: sorts[0].colId, direction: sortDirection});
    }

    return sortDTOs;
  }

  generateHeader(): Headers {
    var headers = new Headers({'Content-Type': 'application/json'});
    return headers;
  }

  initConfigurationToastMessage(message: string,
    title: string,
    navigationLinkAfterMessageRemoved: string,
    router: Router): ToastOptions {
    let toastOptions = {
      showClose: true,
      timeout: Config.TOAST_MESSAGE_TIMEOUT,
      theme: Config.toastTheme,
      msg: message,
      title: title,
      onRemove: (toast: ToastData) => {
        if (navigationLinkAfterMessageRemoved !== '') {
          router.navigate([navigationLinkAfterMessageRemoved]);
        }
      }
    };
    return toastOptions;
  }

  parseTsDate(date: any): any {
    if (null === date || undefined === date) {
      return null;
    }
    let tmp: Date = new Date(date);
    let dateModel: any = {
      'year': tmp.getUTCFullYear(),
      'month': tmp.getMonth() + 1,
      'day': tmp.getDate()
    };
    return dateModel;
  }

  parseJVDate(date: any): any {
    if (null === date || undefined === date) {
      return null;
    }
    if ((date.day.toString().length > 2 || date.day < 0 || date.day > 31)) {
      return null;
    }
    if ((date.month.toString().length > 2 || date.month < 0 || date.month > 12)) {
      return null;
    }
    if ((date.year.toString().length !== 4 || date.year < 0)) {
      return null;
    }
    return date.year + '-' + date.month + '-' + date.day;
  }

  deepCopy(oldObj: any) {
    var newObj = oldObj;
    if (oldObj && (typeof oldObj === 'object')) {
      newObj = Object.prototype.toString.call(oldObj) === '[object Array]' ? [] : {};
      for (var i in oldObj) {
        if (oldObj.hasOwnProperty(i)) {
          newObj[i] = this.deepCopy(this.isValid(oldObj[i]) ? oldObj[i].valueOf() : oldObj[i]);
        }
      }
    }
    return newObj;
  }

  downloadFile(data: Blob) {
    var url = window.URL.createObjectURL(data);
    window.open(url);
  }

  downloadFileLink(data: Blob) {
    var url = window.URL.createObjectURL(data);
    var link = document.createElement('a');
    link.href = url;
    link.download = UUID.UUID();
    link.click();
  }

  translate(translate: TranslateService, key: string, strs: string[]): Observable<Array<any>> {
    if (null == strs || 0 === strs.length) {
      return null;
    }
    let tmpKey: string[] = [];
    for (let str in strs) {
      tmpKey.push(key + strs[str]);
    }
    return translate.get(tmpKey);
  }

  buildFieldOption(key: string, strs: string[], trans: any[]): Array<FieldObj> {
    let result: FieldObj[] = [];
    for (let i in strs) {
      let field: FieldObj = new FieldObj();
      field.fieldName = strs[i];
      let tmp: string = key + strs[i];
      for (let j in trans) {
        if (tmp === j) {
          field.fieldTitle = trans[j];
        }
      }
      result.push(field);
    }
    return result;
  }

  parseSelectValues(items: DropdownListItem[]) {
    let results: any[] = [];
    if (null == items) {
      return results;
    }
    for (let item of items) {
      results.push(item.id);
    }
    return results;
  }

  selectValue(id: any, items: DropdownListItem[]) {
    let results: any[] = [];
    if (null == items || null === id) {
      return results;
    }
    for (let item of items) {
      if (id === item.id) {
        results.push(item);
      }
    }
    return results;
  }

  selectValues(items: DropdownListItem[]) {
    let results: any[] = [];
    if (null == items) {
      return results;
    }
    for (let item of items) {
      results.push(item.id);
    }
    return results;
  }
}
