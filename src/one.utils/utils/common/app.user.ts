
export class AppUser {
  id: string;
  username: string;
  fullName: string;
  roles: string[];
  apiFunctions: any[];
  menuFunctions: any[];
}
