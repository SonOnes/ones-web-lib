import {Config} from '../../../config';
import {TranslateService} from '@ngx-translate/core';

export class AppUser {
  id: string;
  username: string;
  fullName: string;
  roles: string[];
  apiFunctions: any[];
  menuFunctions: any[];
}

export class DropdownListItem {
  public id: any;
  public itemName: String;
  constructor(id: any, itemName: string) {
    this.id = id;
    this.itemName = itemName;
  }
}

export class DropdownList {
  public items: DropdownListItem[] = [];
  constructor(strEnum: string[], objs: any[]) {

    if (null != strEnum && 0 < strEnum.length) {
      for (let str of strEnum) {
        this.items.push(new DropdownListItem(str, str));
      }
      return;
    }
    if (null != objs && 0 < objs.length) {
      for (let obj of objs) {
        this.items.push(new DropdownListItem(obj.id, obj.name));
      }
    }
  }

  public toValue(items: DropdownListItem[]) {
    let results: any[] = [];
    if (null == items) {
      return results;
    }
    for (let item of items) {
      results.push(item.id);
    }
    return results;
  }
}

export class Dropdown {
  itemsSelected: DropdownListItem[] = [];
  listItem: DropdownListItem[] = [];
  settings = Config.multiDropdownSettings;
  constructor() {
    this.itemsSelected = [];
    this.listItem = [];
    this.settings = Config.multiDropdownSettings;
  }
}

export class FieldObj {
  fieldName: string;
  fieldTitle: string;
}

export class GridColumn {
  colId: string;
  headerName: string;
  width: number;
  checkboxSelection: boolean;
  suppressSorting: boolean;
  suppressMenu: boolean;
  pinned: any;
  template: string;
  field: string;
  valueFormatter: any;
  public static key: string;
  public static translate: TranslateService;

  constructor(id: string, field: string, width: number, checkboxSelection: boolean, suppressSorting: boolean,
    suppressMenu: boolean, pinned: any, template: string, valueFormatter: any) {
    let columnName: string = '';
    if (null != field) {
      GridColumn.translate.get(GridColumn.key + field).subscribe(data => {
        columnName = data;
      });
    }
    this.colId = id;
    this.checkboxSelection = checkboxSelection;
    this.headerName = columnName;
    this.field = field;
    this.pinned = pinned;
    this.suppressMenu = suppressMenu;
    this.suppressSorting = suppressSorting;
    this.template = template;
    this.width = width;
    this.valueFormatter = valueFormatter;
  }
}

export class OnesSort {
    property: string;
    direction: string;
}

export class OnesMessage {
    code: string;
    message: string;
    type: string;
    violations: any[];
}

export class OnesPaging<T> {
    first: boolean;
    last: boolean;
    number: number;
    numberOfElements: number;
    size: number;
    // sort: any;
    totalElements: number;
    totalPages: number;
    content: T[];

    constructor(data: any) {
        this.first = data.first;
        this.last = data.last;
        this.number = data.number;
        this.numberOfElements = data.numberOfElements;
        this.size = data.size;
        this.totalElements = data.totalElements;
        this.totalPages = data.totalPages;
        this.content = data.content;
    }
}

export class Typeahead {
  public searching: boolean = false;
  public searchNoData: boolean = false;
  public inputSuggestFormatter: any;
  public formatterSuggestResult: any;
  public suggestion: any;
  public model: any;
}

