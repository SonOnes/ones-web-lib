import {DropdownListItem} from './dropdown.list.item';
import {Config} from '../../../config';

export class Dropdown {
  itemsSelected: DropdownListItem[] = [];
  listItem: DropdownListItem[] = [];
  settings = Config.multiDropdownSettings;
  constructor() {
    this.itemsSelected = [];
    this.listItem = [];
    this.settings = Config.multiDropdownSettings;
  }
}
