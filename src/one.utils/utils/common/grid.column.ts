import {TranslateService} from '@ngx-translate/core';

export class GridColumn {
  colId: string;
  headerName: string;
  width: number;
  checkboxSelection: boolean;
  suppressSorting: boolean;
  suppressMenu: boolean;
  pinned: any;
  template: string;
  field: string;
  valueFormatter: any;
  public static key: string;
  public static translate: TranslateService;

  constructor(id: string, field: string, width: number, checkboxSelection: boolean, suppressSorting: boolean,
    suppressMenu: boolean, pinned: any, template: string, valueFormatter: any) {
    let columnName: string = '';
    if (null != field) {
      GridColumn.translate.get(GridColumn.key + field).subscribe(data => {
        columnName = data;
      });
    }
    this.colId = id;
    this.checkboxSelection = checkboxSelection;
    this.headerName = columnName;
    this.field = field;
    this.pinned = pinned;
    this.suppressMenu = suppressMenu;
    this.suppressSorting = suppressSorting;
    this.template = template;
    this.width = width;
    this.valueFormatter = valueFormatter;
  }
}
