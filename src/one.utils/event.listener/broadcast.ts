import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';

export interface IBroadcast {
	key: EVENT;
	data?: any;
}

export class Broadcast {
	private _eventBus: Subject<IBroadcast>;

	constructor() {
		this._eventBus = new Subject<IBroadcast>();
	}

	broadcast(key: EVENT, data?: any) {
		this._eventBus.next({ key, data });
	}

	on<T>(key: EVENT): Observable<T> {
		return this._eventBus.asObservable()
			.filter(event => event.key === key)
			.map(event => <T>event.data);
	}
}

export const enum EVENT {
    SHOW_SPINNER,
    HIDE_SPINNER,
	SHOW_CONFIRM_DIALOG,
	HIDE_CONFIRM_DIALOG
}
