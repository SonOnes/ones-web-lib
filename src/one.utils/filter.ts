import {Pipe, PipeTransform, Injectable} from '@angular/core';

@Pipe({ name: 'exponentialStrength' })
export class ExponentialStrengthPipe implements PipeTransform {
	transform(value: any, exponent: any[]): any {
		if (!value) {
			return value;
		}

		for (let i = 0; i < exponent.length; i++) {
			if (exponent[i].id === value) {
				return exponent[i].translate;
			}
		}
		return value;
	}
}
